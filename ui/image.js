import THEKIT from "nodejs-thekit";
import OneKit from "../x2nut5g/OneKit.js"
import html_entities from 'html-entities';
const {
  decode
} = html_entities
const {
  PATH,
  URL
} = THEKIT;
import ui from "./ui.js"
export default class onekit_image extends ui {
  constructor(context) {
    super(context)
  }

  async render(parent_mix) {
    if (!this.src) {
      return {
        image: null,
        title: null
      }
    }
    let url = this.src
    if(url){
      url = decode(url, {
        level: 'all'
      })
    }
    const data = {
      image: null,
      title: null
    }
    //console.log(".........",this.context.route, url,PATH.rel2abs(this.context.route, url))
    url = PATH.rel2abs(this.context.route, url)
    this.tag = url
    if (parent_mix) {
      if (this.needUpdate()) {
        data.image = url
      }
      //console.log("uuuuuuuuuuu",url,this.needUpdate(),data)
      this.context.renderOnce[this.tag] = true
      return data
    }
    this.tag = url
    const {
      self_mix
    } = this._postback(parent_mix)
    if (self_mix) {
      if (this.needUpdate()) {
        data.image = url
        this.context.renderOnce[this.tag] = true
        await OneKit.session().fastsdk.msg_card(data)
      }
    } else {
      if (this.needUpdate()) {
        this.context.renderOnce[this.tag] = true
        await OneKit.session().fastsdk.msg_image(url)
      }
    }
    return null
  }
}

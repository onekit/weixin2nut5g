import ui from "./ui.js"
import OneKit from "../x2nut5g/OneKit.js"
export default class onekit_literal extends ui {
  constructor(context, literal) {
    super(context)
    this.literal = literal;
  }

  async render(parent_mix) {
    if (parent_mix) {
      return {
        image: null,
        title: this.literal
      }
    }
    this.tag = this.literal
    if (this.needUpdate()) {
      await OneKit.session().fastsdk.msg_text(this.literal)
    }
    this.context.renderOnce[this.tag] = true
  }
}

import ui from "./ui.js"
import TheKit from "nodejs-thekit"
const {
  PATH
} = TheKit
export default class onekit_navigator extends ui {
  constructor(context) {
    super(context)
    this.openType = "navigate"
  }
  async render(parent_mix) {
    let type

    switch (this.openType) {
      case "navigate":
        type = "navigateTo"
        break;
      case "redirect":
        type = "redirectTo"
        break;
      case "switchTab":
        type = "switchTab"
        break;
      case "reLaunch":
        type = "reLaunch"
        break;
      case "navigateBack":
        type = "navigateBack"
        break;
      case "exit":
        type = "exit"
        break;
      default:
        throw this.openType
    }
    let postback = {
      type,
      info: {
        url: this.url ? PATH.rel2abs(this.context.route, this.url) : null
      }
    }
    const self_mix = !parent_mix
    return await this.mix(parent_mix, self_mix, postback)
  }
}

import ui from "./ui.js"
import OneKit from "../x2nut5g/OneKit.js"
export default class onekit_for extends ui {
  constructor(context) {
    super(context)
  }
  async render() {
    //  console.log("ccccccccccccccc",this.children.length)
    const datas = []
    for (const child of this.children) {
      const data = await child.render(true)
      datas.push(data)
    }
    if (this.needUpdate()) {
      await OneKit.session().fastsdk.msg_cards(datas, "")
    }
    return {}
  }

}

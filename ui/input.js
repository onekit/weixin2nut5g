import formItem from "./formItem.js"
export default class onekit_input extends formItem {
  constructor(context) {
    super(context)
    this.placeholder = "请输入"
  }
  render() {
    return {
      title: `(${this.placeholder})`
    }
  }

}

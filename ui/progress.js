import ui from "./ui.js"
export default class onekit_progress extends ui {
  constructor(context) {
    super(context)
  }
  set percent(percent) {
    this._percent = percent
  }
  get percent() {
    return this._percent
  }


  set showInfo(showInfo) {
    this._showInfo = showInfo
  }
  get showInfo() {
    return this._showInfo
  }



  set borderRadius(borderRadius) {
    this._borderRadius = borderRadius
  }
  get borderRadius() {
    return this._borderRadius
  }

  set fontSize(fontSize) {
    this._fontSize = fontSize
  }
  get fontSize() {
    return this._fontSize
  }

  set strokeWidth(strokeWidth) {
    this._strokeWidth = strokeWidth
  }
  get strokeWidth() {
    return this._strokeWidth
  }

  set color(color) {
    this._color = color
  }
  get color() {
    return this._color
  }

  set activeColor(activeColor) {
    this._activeColor = activeColor
  }
  get activeColor() {
    return this._activeColor
  }
  set backgroundColor(backgroundColor) {
    this._backgroundColor = backgroundColor
  }
  get backgroundColor() {
    return this._backgroundColor
  }

  set active(active) {
    this._active = active
  }
  get active() {
    return this._active
  }

  set activeMode(activeMode) {
    this._activeMode = activeMode
  }
  get activeMode() {
    return this._activeMode
  }

  set duration(duration) {
    this._duration = duration
  }
  get duration() {
    return this._duration
  }

  set bindactiveend(bindactiveend) {
    this._bindactiveend = bindactiveend
  }
  get bindactiveend() {
    return this._bindactiveend
  }




}

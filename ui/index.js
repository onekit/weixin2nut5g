import block from './block.js'
import ad from './ad.js'
import ad_custom from './ad_custom.js'
import audio from './audio.js'
import button from './button.js'
import camera from './camera.js'
import canvas from './canvas.js'
import checkbox_group from './checkbox_group.js'
import checkbox from './checkbox.js'
import cover_image from './cover_image.js'
import cover_view from './cover_view.js'
import editor from './editor.js'
import form from './form.js'
import functional_page_navigator from './functional_page_navigator.js'
import icon from './icon.js'
import include from './include.js'
import image from './image.js'
import input from './input.js'
import lable from './lable.js'
import literal from './literal.js'
import live_player from './live_player.js'
import live_pusher from './live_pusher.js'
import map from './map.js'
import match_media from './match_media.js'
import movable_area from './movable_area.js'
import movable_view from './movable_view.js'
import navigation_bar from './navigation_bar.js'
import navigator from './navigator.js'
import official_account from './official_account.js'
import open_data from './open_data.js'
import page_meta from './page_meta.js'
import picker_view_column from './picker_view_column.js'
import picker_view from './picker_view.js'
import picker from './picker.js'
import progress from './progress.js'
import radio_group from './radio_group.js'
import radio from './radio.js'
import rich_text from './rich_text.js'
import scroll_view from './scroll_view.js'
import slider from './slider.js'
import swiper_item from './swiper_item.js'
import swiper from './swiper.js'
import text from './text.js'
import textarea from './textarea.js'
import video from './video.js'
import view from './view.js'
import voip_room from './voip_room.js'
import web_view from './web_view.js'
export default {
  block,
  ad,
  ad_custom,
  audio,
  button,
  camera,
  canvas,
  checkbox_group,
  checkbox,
  cover_image,
  cover_view,
  editor,
  form,
  functional_page_navigator,
  icon,
  include,
  image,
  input,
  lable,
  literal,
  live_player,
  live_pusher,
  map,
  match_media,
  movable_area,
  movable_view,
  navigation_bar,
  navigator,
  official_account,
  open_data,
  page_meta,
  picker_view_column,
  picker_view,
  picker,
  progress,
  radio_group,
  radio,
  rich_text,
  scroll_view,
  slider,
  swiper_item,
  swiper,
  text,
  textarea,
  video,
  view,
  voip_room,
  web_view
}

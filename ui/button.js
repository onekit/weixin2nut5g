import ui from "./ui.js"
export default class onekit_button extends ui {
  constructor(context) {
    super(context)
    this.size = "default"
    this.type = "default"
    this.plain = false
    this.disabled = false
    this.loading = false
    this.hoverClass = "button-hover"
    this.hoverStopPropagation = false
    this.hoverStartTime = 20
    this.hoverStayTime = 70
    this.lang = "en"
    this.showMessageCard
  }
  _ui_postback
  async render(parent_mix) {
    let postback = {}
    if (this.openType) {
      const systemHandler = `onekit_${this.id}`
      switch (this.openType) {
        case "getUserInfo":
          this.innerHandlers.bindTap = this.innerHandlers.catchtap = () => {
            console.log("wwwwwwwwwww")
          }
          break;
        default:
          throw this.openType
      }
    }
    const self_mix = !parent_mix && this.openType
    return await this.mix(parent_mix, self_mix, postback)
  }
}

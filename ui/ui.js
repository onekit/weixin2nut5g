import TheKit from "nodejs-thekit"
const {
  STRING,
  OBJECT
} = TheKit
import OneKit from "../x2nut5g/OneKit.js"
export default class ui {
  get class() {
    return this.constructor.name.substr("onekit_".length)
  }
  constructor(context) {
    if (context) {
      this.context = context
    }
    this.eventNames = ["Tap"]
    this.dataset = {}
    //
    this.children = []
    this.indexPath = []
    //
    this.style = ""
    this.className = ""
    //
    this.innerHandlers = {}
  }
  invokeInnterHandler(event, detail) {
    const handler = this.innerHandlers[event]
    if (handler) {
      handler({
        detail
      });
    }
  }
  _postback(parent_mix) {
    if (parent_mix) {
      return {
        self_mix: false
      }
    }
    if (this.formItem) {
      switch (this.formItem.class) {
        case "input":
        case "textarea":
          this.context.formRowCallback = (content) => {
            this.formItem.formValue = content
            const e = {
              currentTarget: this.formItem,
              detail: {
                value: content
              }
            }
            if (this.formItem.bindInput) {
              this.context[this.formItem.bindInput](e);
            } else if (this.formItem.catchinput) {
              this.context[this.formItem.catchinput](e);
            }
          }
          return {
            self_mix: true
          }
          default:
            throw this.formItem.class
      }

    }
    let self_mix = false
    let postback
    for (const prex of ['bind', 'catch']) {
      for (const eventName of this.eventNames) {
        const event = `${prex}${eventName}`
        if (this[event]) {
          postback = {
            ui_id: this.id,
            type: "event",
            event,
            page_id: this.context.id,
            handler: this[event]
          }
          //console.log("eeeeeeee", eventName, event)
          self_mix = true
          break
        }
      }
      if (self_mix) {
        break
      }
    }
    return {
      self_mix,
      postback
    }
  }
  needUpdate() {
    if (this.keys == null) {
      //  console.log("aaaaaaaaaaaa", this.keys)
      return !this.context.renderOnce[this.tag]
    }
    for (const key of this.keys) {
      const paths = key.split(".")
      //let temp = OBJECT.clone(this.data)
      let temp = this.data
      if (!temp) {
        return false
      }
      for (let p = 0; p < paths.length - 1; p++) {
        const path = paths[p]
        temp = temp[path]
        if (!temp) {
          //  console.log("xxxxxxxx", key, this.data)
          return false
        }
      }
      if (Object.keys(temp).includes(paths[paths.length - 1])) {
        return true
      }
    }
    return false
  }
  async render(parent_mix) {
    if (this.context.pause) {
      return {}
    }
    if (this.hidden) {
      console.log(this.indexPath)
      return {};
    }
    if (this.context.OnekitCSS.View_H5.getDisplay(this) == "none") {
      return {};
    }
    let {
      self_mix,
      postback
    } = this._postback(parent_mix)

    return await this.mix(parent_mix, self_mix, postback)
  }
  async mix(parent_mix, self_mix, postback) {
    const result = {
      image: null,
      title: null,
      description: "",
      postback
    }
    ///////////////////////////////////////
    if (this.formItem && !this.formItem.value) {
      var rowText = "【请按提示输入】\n";
      for (const child of this.children) {
        const data = await child.render(true)

        // console.log("hhhhhhhhh",data)
        if (data) {
          if (data.title) {
            rowText += `${data.title}`;
          }
          if (data.description) {
            rowText += `${data.description}`
          }
        }
      }
      await OneKit.session().fastsdk.msg_text(rowText)
      this.context.pause = true
      return result
    }
    ///////////////////////////////////////
    const unit_value = this.context.OnekitCSS.View_H5.getSize("height", this.computedStyle.height)
    if (unit_value != null && unit_value[1] == 0) {
      return result;
    }
    if (!this.needUpdate()) {
      return result;
    }
    for (const child of this.children) {
      const data = await child.render(parent_mix || self_mix)

      // console.log("hhhhhhhhh",data)
      if (data && (parent_mix || self_mix)) {
        if (!result.image && data.image) {
          result.image = data.image
        }
        if (data.title) {
          if (!result.title) {
            result.title = data.title;
          } else {
            result.description += `${data.title}\n`
          }
        }
        if (data.description) {
          result.description += `${data.description}\n`
        }
        if (data.postback) {
          result.postback = data.postback
        }
      }
    }
    if (!parent_mix && self_mix) {
      await OneKit.session().fastsdk.msg_card(result)
      return {}
    } else {
      return result
    }
  }
  appendChild(child) {
    if (!child.id) {
      child.id = STRING.guid()
    }
    this.context.uis[child.id] = child
    child.parent = this
    //
    child.index = this.children.length
    //
    child.dataset = {}
    for (const key of Object.keys(child)) {
      if (key.startsWith("data-")) {
        child.dataset[key.substr("data-")] = child[key]
      }
    }
    //
    this.children.push(child)
  }
}

import OneKit from "./x2nut5g/OneKit.js"

export default {
  getCurrentPages() {
    return OneKit.session().currentPages
  },
  getApp() {
    return OneKit.session().app
  }
}

import TheKit from "nodejs-thekit"
import fs from "fs"
import os from "os"
import path from "path"
const {
  FILE,
  URL,
  AJAX,
  STRING
} = TheKit
const __dirname = path.resolve(path.dirname(''));
export default class OneKit {
  static session() {
    const address = global.address
    if (!process[address]) {
      process[address] = {
        currentPages: [],
        pages: {},
        app: global.app
      }
    }
    return process[address]
  }
  static phone2address(phone) {
    return `tel:${phone}`
  }
  static address2phone(address) {
    const phone = address.split(":")[1]
    const nation = "86"
    return phone.substring(phone.indexOf('nation') + nation.length)
  }
  static wxurl2nut5g(wx_url) {
    throw new Error('[X2Nut5G-Util-getResource] todo!')
  }
  static async _down(url, temp) {
    //console.log("llllllllllll", url)
    return new Promise((callback) => {
      AJAX.download(callback, temp, url)
    })
  }
  static async url2res(url) {

    const medias_path = `${__dirname}/onekit/data/medias.json`
    const medias = JSON.parse(fs.readFileSync(medias_path))
    const res = medias[url]
    console.log("xxxxxxxxxxxxxxxxxxxx", url, res)
    if (res == null) {
      if (!new URL(url).host) {
        return null
      }
      const p = url.lastIndexOf(".")
      const f = url.lastIndexOf("/")
      const ext = (p >= 0 && p > f) ? url.substr(p) : ".jpg"
      const temp = `${os.homedir()}/${nut5gAppId}/${STRING.guid()}${ext}`
      //  console.log("ooooooooooo", url)
      await this._down(url, temp)
      medias[url] = await OneKit.session().fastsdk.upload(url, temp)
      fs.writeFileSync(medias_path, JSON.stringify(medias))
      fs.unlinkSync(temp)
    }
    return res
  }
}

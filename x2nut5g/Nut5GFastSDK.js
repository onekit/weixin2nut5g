import TheKit from "nodejs-thekit";
import path from "path";
import fs from "fs";
import OneKit from "./OneKit.js";
import nut5g_cloud from "nut5g-cloud";
import nut5g_cloud_sdk from "nut5g-cloud-sdk"
const {
  STRING,
  FileDB,
  TASK
} = TheKit
const __dirname = path.resolve(path.dirname(''));
const {
  ChatbotInfoMenuRequest,
  AccessTokenRequest,
  MessagesRequest,
  MessagesRevokeRequest
} = nut5g_cloud.com.msg5g.maap.request
const {
  Suggestion,
  Reply,
  Content,
  Layout
} = nut5g_cloud.com.msg5g.maap.request.entry
const {
  Nut5GSDK
} = nut5g_cloud_sdk.com.msg5g.maap
/*
const {
  MessageNotificationSDK
} = nut5g_cloud_sdk.com.msg5g.maap.notification
*/
class Nut5GFastSDK {
  constructor(account, address) {
    this.account = account
    this.address = address
    this.sdk = new Nut5GSDK(account)
    OneKit.session().accessToken_callbacks = [];
    this.messageIds = []
  }
  async _checkAccessToken() {
    return new Promise((callback) => {
      let accessToken = process.accessToken
      let accessToken_expire = process.accessToken_expire
      if (accessToken && (new Date().getTime() < accessToken_expire)) {
        // console.log("tttttttttttttttt2",process.accessToken,new Date().getTime() , process.accessToken_expire)
        callback(accessToken);
        return
      }
      console.log("!!!!!!!!!!!!!!!!!!", accessToken, new Date().getTime() < accessToken_expire)
      const request = new AccessTokenRequest();
      request.appId = this.account.appid;
      request.appKey = this.account.appKey;

      this.sdk.accessToken(request, (response) => {
        const accessToken = response.accessToken
        const accessToken_expire = new Date().getTime() + response.expires * 1000
        console.log("accessToken,accessToken_expire", accessToken, accessToken_expire)
        process.accessToken = accessToken
        process.accessToken_expire = accessToken_expire
        //console.log("tttttttttttttttt1",process.accessToken,response.expires)
        callback(accessToken);
      });
    })
  }
  async msg_revokes() {
    for (const messageId of this.messageIds) {
      await this.msg_revoke(messageId)
    }
    this.messageIds = []
  }
  async msg_revoke(messageId) {
    const accessToken = await this._checkAccessToken()

    return new Promise((callback) => {
      const messagesRevokeRequest = new MessagesRevokeRequest()
      messagesRevokeRequest.messageId = messageId;
      const list = []
      list.push(this.address);
      messagesRevokeRequest.destinationAddress = list;
      this.sdk.messagesRevoke(accessToken, messagesRevokeRequest, () => {
        callback()
      });

    })
  }
  async upload(url, file, mode = "temp") {
    const accessToken = await this._checkAccessToken()
    console.log("upload===========", accessToken, url, file)
    return new Promise((callback) => {
      this.sdk.mediasUpload(accessToken, "temp", {
        "file1": file
      }, (response) => {
        const fileInfo = response.fileInfo[0]
        console.log("fileInfo=============", fileInfo)
        //
        const medias_path = `${__dirname}/onekit/data/medias.json`
        const medias = JSON.parse(fs.readFileSync(medias_path))
        medias[url] = fileInfo
        fs.writeFileSync(medias_path, JSON.stringify(medias))
        //
        callback(fileInfo)
      });
    })
  }
  async msg_send(message) {
    const accessToken = await this._checkAccessToken()

    return new Promise((callback) => {
      const request = new MessagesRequest();
      request.messageId = STRING.guid()
      const destinationAddress = [];
      destinationAddress.push(this.address)
      request.destinationAddress = destinationAddress;
      request.contributionId = this.contributionId
      request.conversationId = this.conversationId
      const serviceCapability = new MessagesRequest.ServiceCapability()
      serviceCapability.version = '+g.gsma.rcs.botversion="#=1"'
      request.serviceCapability = [serviceCapability];
      request.messageList = [message];
      //
      this.sdk.messages(accessToken, request, (res) => {
        //    console.log("ooooooooooo",res)
        this.messageIds.push(res.messageId)
        callback()
      });
    })
  }
  async msg_text(text) {
    //  return
    const textMessage = new MessagesRequest.TextMessage()
    textMessage.contentText = text;
    textMessage.contentEncoding = "utf8";
    //
    console.log("====================== text", textMessage)
    await this.msg_send(textMessage)
  }
  async msg_image(url) {
    //return
    //////////////////////////////
    const contentText1 = await OneKit.url2res(url)
    console.log("~~~~~~~~~~~[msg_image]", url, contentText1)
    contentText1.type = "thumbnail"
    const contentText2 = await OneKit.url2res(url)
    contentText2.type = "file"
    //
    const fileMessage = new MessagesRequest.FileMessage()
    const contentTextlist = []
    contentTextlist.push(contentText1);
    contentTextlist.push(contentText2);
    fileMessage.contentText = contentTextlist;
    fileMessage.contentEncoding = "utf8"
    //console.log(fileMessage)
    console.log("====================== file", fileMessage)
    await this.msg_send(fileMessage)
  }
  async msg_card({
    image,
    title,
    description,
    postback
  }) {
    if (!image && !title && !description) {
      return
    }
    ///////卡片消息////////////////////
    const botMessage = new MessagesRequest.BotMessage;
    const contentText = new MessagesRequest.BotMessage.ContentText;
    const cssMessage = new MessagesRequest.BotMessage.ContentText.RichMessage;
    const generalPurposeCard = new MessagesRequest.BotMessage.ContentText.RichMessage.GeneralPurposeCard;
    const content = new Content;
    const layout = new Layout;
    if (image && title && description) {
      layout.cardOrientation = "HORIZONTAL";
      layout.imageAlignment = "LEFT";
      const titleFontStyle = []
      //  titleFontStyle.push("underline")
      titleFontStyle.push("bold")
      layout.titleFontStyle = titleFontStyle
      //  const descriptionFontStyle = []
      //  descriptionFontStyle.push("calibri")
      //  layout.descriptionFontStyle = descriptionFontStyle
      //  layout.style = "http://example.com/default.css";
    }
    generalPurposeCard.layout = layout;
    if (image) {
      const res = await OneKit.url2res(image)
      const res_mini = await OneKit.url2res(image)
      //console.log("~~~~~~~~~~~",res)
      const media = new Content.Media;
      media.mediaUrl = res.url;
      media.mediaContentType = res.contentType;
      media.mediaFileSize = res.fileSize;
      media.thumbnailUrl = res_mini.url;
      media.thumbnailContentType = res_mini.contentType;
      media.thumbnailFileSize = res_mini.fileSize;
      media.height = "SHORT_HEIGHT";
      //media.contentDescription = "Textual description of media content, e. g. for use with screen readers";
      content.media = media;
    }
    //
    if (title) {
      content.title = title;
    }
    if (description) {
      content.description = description;
    }
    ///////////////////////
    if (postback) {
      const suggestion1 = new Suggestion;
      const reply1 = new Reply(STRING.stringToBase64(JSON.stringify(postback)), "查看");
      //console.log("rrrrrrrrrrrrr", reply1)
      suggestion1.reply = reply1
      const suggestions = []
      suggestions.push(suggestion1)
      content.suggestions = suggestions
    }

    generalPurposeCard.content = content;

    cssMessage.generalPurposeCard = generalPurposeCard;
    contentText.message = cssMessage;

    botMessage.contentText = contentText;
    //if (this.flag == 2) {
    console.log("====================== card", image, title, postback ? STRING.stringToBase64(JSON.stringify(postback)) : "", postback)
    await this.msg_send(botMessage)
    //}
  }
  async msg_cards(datas,isBig) {
    // console.log("xxxxxxx", datas)
    if (datas.length <= 0) {
      return
    }
    const CARDS_SIZE = 10
    const cardsCount = Math.ceil(datas.length / CARDS_SIZE)
    let index = 0;
    for (let n = 0; n < cardsCount; n++) {
      const BotMessage = new MessagesRequest.BotMessage();
      const contentText = new MessagesRequest.BotMessage.ContentText();
      const cssMessage = new MessagesRequest.BotMessage.ContentText.RichMessage();
      const generalPurposeCardCarousel = new MessagesRequest.BotMessage.ContentText.RichMessage.GeneralPurposeCardCarousel();
      const layout = new Layout();
      layout.cardWidth = "MEDIUM_WIDTH";
      generalPurposeCardCarousel.layout = layout;
      const contentList = []
      for (let i = 0; i < CARDS_SIZE; i++, index++) {
        if (index >= datas.length) {
          break
        }
        const data = datas[index]
        const {
          image,
          title,
          description,
          postback
        } = data
        if (!image && !title) {
          continue
        }
        const content = new Content();

        if (image) {
          const res = await OneKit.url2res(image)
          const res_mini = await OneKit.url2res(image)
          //console.log("~~~~~~~~~~~",image,res)
          const media = new Content.Media;
          media.mediaUrl = res.url;
          media.mediaContentType = res.contentType;
          media.mediaFileSize = res.fileSize;
          media.thumbnailUrl = res_mini.url;
          media.thumbnailContentType = res_mini.contentType;
          media.thumbnailFileSize = res_mini.fileSize;
          media.height = "SHORT_HEIGHT";
          //media.contentDescription = "Textual description of media content, e. g. for use with screen readers";
          content.media = media;
        }
        //
        if (title) {
          content.title = title;
        }
        if (description) {
          content.description = description;
        }
        ///////////////////////
        if (postback) {
          const suggestions = []
          const suggestion1 = new Suggestion;
          const reply1 = new Reply(STRING.stringToBase64(JSON.stringify(postback)),
            "查看");
          suggestion1.reply = reply1
          suggestions.push(suggestion1)
          content.suggestions = suggestions
        }
        contentList.push(content)
        console.log("====================== cards", data, content, content.suggestions ? content.suggestions[0] : "")
      }
      if (contentList.length <= 0) {
        return
      }
      generalPurposeCardCarousel.content = contentList;


      cssMessage.generalPurposeCardCarousel = generalPurposeCardCarousel;
      contentText.message = cssMessage;

      BotMessage.contentText = contentText;
      ////////////////////
      console.log("====================== cards =============")
      await this.msg_send(BotMessage)
    }
  }
  postback(data) {
    if (data.messageList.length <= 0) {
      return null
    }
    const message = data.messageList[0]
    if (!message.contentText.response) {
      console.log("------------------ content", message.contentText)
      return message.contentText
    }
    const postback = JSON.parse(STRING.base64ToString(message.contentText.response.reply.postback.data))
    console.log("------------------ postback", postback)
    return postback;
  }
}
export default Nut5GFastSDK

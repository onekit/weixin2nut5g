import OnekitCSS from "./css2node/core/OnekitCSS.js"
import page from "./ui/page.js"
import TheKit from "nodejs-thekit"
import path from 'path'
import OneKit from "./x2nut5g/OneKit.js"
import formItem from "./ui/formItem.js"

const __dirname = path.resolve(path.dirname(''));
const {
  STRING,
  FILE,
  OBJECT,
  URL
} = TheKit
export default class NodejsPage {
  constructor(url) {
    const uri = new URL(url)
    this.uri = uri
    this.route = uri.path
    this.uis = {}
    this.id = STRING.guid()
    OneKit.session().currentPage = this
    OneKit.session().pages[this.id] = this
    OneKit.session().currentPages.push(this)
    this.inputValues = {}
    this.ONEKIT_JS = import(`${__dirname}/${this.route}.js`);
    this.ONEKIT_UI = import(`${__dirname}/${this.route}.wxml.js`);
    this.ONEKIT_STYLE = import(`${__dirname}/${this.route}.wxss.js`);
    this.ONEKIT_JSON = import(`${__dirname}/${this.route}.json.js`);
    this.renderOnce = {}
  }
  async run() {
    const JS = (await this.ONEKIT_JS).default;
    for (const key of Object.keys(JS)) {
      this[key] = JS[key]
    }
    if (!this.data) {
      this.data = {}
    }

    global.current = await this.renderPage(this.data)
    /////////////
    if (this.onLoad) {
      await this.onLoad(this.uri.params)
    }
    if (this.onReady) {
      await this.onReady()
    }
    if (this.onShow) {
      await this.onShow()
    }
  }
  async renderPage(DATA) {
    ////////////////////////////
    this.prev = __dirname
    await OneKit.session().fastsdk.msg_revokes()
    const root = new page(this);
    (await this.ONEKIT_UI).default(this, root, DATA)
    this.OnekitCSS = new OnekitCSS({
      width: 750,
      height: 1334
    }, path.join(__dirname, '/'))
    this.OnekitCSS.run(root, ["/app.css"], (await this.ONEKIT_STYLE).default, this.route)
    /////////
    this.findFormItems(root)
    ////////
    await root.render(false)
  }
  findFormItems(root) {
    this.pause = false
    const formItemWithPathIndexs = []
    //
    const findNode = (parent) => {
      for (const child of parent.children) {
        child.indexPath = parent.indexPath.concat([child.index])
        if (this.OnekitCSS.View_H5.getDisplay(child) == "none") {
          continue;
        }
        if (child instanceof formItem) {
          formItemWithPathIndexs.push({
            formItem: child,
            indexPath: child.indexPath
          })
        }
        findNode(child)
      }
    }
    findNode(root)
    //
    if (formItemWithPathIndexs.length <= 1) {
      return;
    }
    const formRowIndexPaths = []
    const addFormRow = (rowIndexPath, formItem) => {
      for (const formRowIndexPath of formRowIndexPaths) {
        if (formRowIndexPath.toString() == rowIndexPath.toString()) {
          return
        }
      }
      formRowIndexPaths.push(rowIndexPath)
      //
      let temp = root
      for (const p of rowIndexPath) {
        temp = temp.children[p]
      }
      temp.formItem = formItem
    }
    for (var i = 0; i < formItemWithPathIndexs.length; i++) {
      for (var j = i + 1; j < formItemWithPathIndexs.length; j++) {
        const formItemWithPathIndex1 = formItemWithPathIndexs[i]
        const formItemWithPathIndex2 = formItemWithPathIndexs[j]
        const indexPath1 = formItemWithPathIndex1.indexPath;
        const indexPath2 = formItemWithPathIndex2.indexPath;
        const length = Math.min(indexPath1.length, indexPath2.length)
        for (var d = 0; d < length; d++) {
          if (indexPath1[d] != indexPath2[d]) {
            addFormRow(indexPath1.slice(0, d + 1), formItemWithPathIndex1.formItem)
            addFormRow(indexPath2.slice(0, d + 1), formItemWithPathIndex2.formItem)
            break;
          }
        }
      }
    }
  }
  async setData(data) {
    //console.log("dddddddddddd", data)
    await OneKit.session().fastsdk.msg_text(`----- ${new Date().toLocaleTimeString()} ----`)
    //  console.log("DDDDDDDDDDDDDD",this.data,data)
    this.lastData = data
    this.data = await Object.assign(this.data, data)
    await this.renderPage(data)
  }
}

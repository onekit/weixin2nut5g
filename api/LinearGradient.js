  import Gradient from "./Gradient.js"
  export default class LinearGradient extends Gradient {

    constructor(x0, y0, x1, y1) {
      super();
      this.info = [
        x0, y0, x1, y1
      ];
    }

    toString() {
      return `type:'linear',points:${JSON.stringify(this.info)},stops:${JSON.stringify(this.colorStops)}`
    }
  }

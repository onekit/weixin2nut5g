export default class NodeRef {
  constructor(selectorQuery, selector, single, component) {
    this._selectorQuery = selectorQuery
    this._selector = selector
    this._single = single
    this._component = component
  }
  boundingClientRect(callback = null) {
    this._selectorQuery._queue.push({
      component: this._component,
      fields: { id: true, dataset: true, rect: true, size: true },
      selector: this._selector,
      single: this._single
    })
    this._selectorQuery._queueCb.push(callback)
    return this._selectorQuery;
  }
  context(callback = null) {
    this._selectorQuery._queue.push({
      component: this._component,
      fields: { context: true },
      selector: this._selector,
      single: this._single
    })
    this._selectorQuery._queueCb.push(callback)
    return this._selectorQuery;
  }
  fields(fields, callback = null) {
    this._selectorQuery._queue.push({
      component: this._component,
      fields: fields,
      selector: this._selector,
      single: this._single
    })
    this._selectorQuery._queueCb.push(callback)
    return this._selectorQuery;
  }
  node(callback = null) {
    this._selectorQuery._queue.push({
      component: this._component,
      fields: { node: true },
      selector: this._selector,
      single: this._single
    })
    this._selectorQuery._queueCb.push(callback)
    return this._selectorQuery;
  }
  scrollOffset(callback = null) {
    this._selectorQuery._queue.push({
      component: this._component,
      fields: { id: true, dataset: true, scrollOffset: true },
      selector: this._selector,
      single: this._single
    })
    this._selectorQuery._queueCb.push(callback)
    return this._selectorQuery;
  }
}

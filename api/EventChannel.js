import EventBus from './EventBus.js'
export default class EventChannel {
  emit(EventBus, args) {
    EventBus.$emit(eventName, args)
  }

  off(EventBus, callback) {
    EventBus.$off(eventName, callback)
  }

  on(EventBus, callback) {
    EventBus.$on(eventName, callback)
  }

  once(EventBus, callback) {
    EventBus.$once(eventName, callback)
  }
}

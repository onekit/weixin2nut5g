import nodejs_thekit from 'nodejs-thekit'
const { PROMISE } = nodejs_thekit
export default class EditorContext {
  constructor(editor) {
    this.editor = editor
  }

  blur(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      this.editor.blur()
      SUCCESS()
    }, success, fail, complete)
  }

  clear(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      this.editor.innerHTML = ''
      SUCCESS()
    }, success, fail, complete)
  }

  format(name, value) {
    const domCss = window.getSelection().focusNode.parentNode.style
    switch (name) {
    case 'strike':
      name = 'strikeThrough'
      break
    case 'ins':
      name = 'underline'
      break
    case 'script':
      if (value === 'sub') name = 'subscript'
      if (value === 'super') name = 'superscript'
      value = null
      break
    case 'header':
      name = 'formatBlock'
      break
    case 'align':
      if (value === 'left') name = 'justifyLeft'
      if (value === 'center') name = 'justifyCenter'
      if (value === 'right') name = 'justifyRight'
      if (value === 'justify') name = 'justifyFull'
      value = null
      break
    case 'direction':
      if (value === 'rtl') name = 'justifyRight'
      break
    case 'indent':
      if (value === '-1') name = 'outline'
      if (value === '+1') name = 'indent'
      value = null
      break
    case 'list':
      if (value === 'ordered ') name = 'insertOrderedList'
      if (value === 'bullet ') name = 'insertUnorderedList'
      value = null
      break
    case 'color':
      name = 'foreColor'
      break
    case 'backgroundColor':
      name = 'hiliteColor'
      break
    default:
      domCss[name] = value
      break
    }
    document.execCommand(name, false, value)
  }

  getContents(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      const regfn = () => {
        const reg = /<[^>]+>/g
        if (reg.test(this.editor.innerHTML)) {
          return this.editor.innerHTML
        } else {
          return `<p>${this.editor.innerHTML}</p>`
        }
      }
      const html = regfn()
      const content = {
        delta: { opts: new Array() },
        errMsg: 'ok',
        html,
        text: this.editor.innerText
      }
      SUCCESS(content)
    }, success, fail, complete)
  }

  getSelectionText(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      let text
      try {
        text = window.getSelection.toString()
      } catch {
        text = this.editor.createRange().text
      }
      SUCCESS(text)
    }, success, fail, complete)
  }

  insertDivider(wx_object) {
    const { success, fail, complete } = wx_object

    PROMISE(SUCCESS => {
      document.execCommand('insertHorizontalRule', false, null)
      SUCCESS()
    }, success, fail, complete)
  }

  insertImage(wx_object) {
    const { src, alt, width, height, extClass, data, success, fail, complete } = wx_object

    PROMISE(SUCCESS => {
      const template = `<img src="${src}" alt="${alt}" width="${width}" height="${height}" class="${extClass}" data="${data}" />`
      document.execCommand('insertHTML', false, template)
      /** 
      const img = document.createElement('img')
      img.setAttribute('src', src)
      img.setAttribute('alt', alt)
      img.setAttribute('width', width)
      img.setAttribute('height', height)
      img.setAttribute('extClass', extClass)
      img.setAttribute('data', data)
      this.editor.appendChild(img)
      */
      SUCCESS()
    }, success, fail, complete)
  }

  insertText(wx_object) {
    const { text, success, fail, complete } = wx_object

    PROMISE(SUCCESS => {
      // this.editor.createRange().text = text
      document.execCommand('insertText', false, text)
      SUCCESS()
    }, success, fail, complete)
  }

  redo(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      document.execCommand('redo', false, null)
      SUCCESS()
    }, success, fail, complete)
  }
  removeFormat(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      if (window.getSelection().baseNode.parentElement) {
        window.getSelection().baseNode.parentElement.style = ''
      }
      document.execCommand('removeFormat', false, null)
      SUCCESS()
    }, success, fail, complete)
  }

  scrollIntoView() {
    this.editor.scrollIntoView()
    this.editor.focus()
  }

  setContents(wx_object) {
    const { html, success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      this.editor.innerHTML = html
      SUCCESS()
    }, success, fail, complete)
  }

  undo(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      document.execCommand('undo', false, null)
      SUCCESS()
    }, success, fail, complete)
  }
}

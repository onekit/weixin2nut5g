import Danmus from './Danmus.js'
export default class VideoContext extends Danmus {

  constructor(videoManager) {
    super()
    this.videoManager = videoManager
  }

  play() {
    this.videoManager.play()
  }

  pause() {
    this.videoManager.pause()
  }

  stop() {
    this.videoManager.pause()
    this.videoManager.currentTime = 0
  }

  seek(position) {
    this.videoManager.currentTime = position
  }

  requestFullScreen() {
    if (this.videoManager.requestFullscreen) {
      this.videoManager.requestFullscreen()
    } else if (this.videoManager.mozRequestFullScreen) {
      this.videoManager.mozRequestFullScreen()
    } else if (this.videoManager.webkitRequestFullScreen) {
      this.videoManager.webkitRequestFullScreen()
    }
  }

  exitFullScreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen()
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen()
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen()
    }
  }

  exitPictureInPicture() {
    console.warn('exitPictureInPicture暂未开放')
  }

  playbackRate(rate) {
    this.videoManager.playbackRate = rate
  }

  sendDanmu(wx_object) {
    const {text, color} = wx_object
    this.send(text, color)
  }

  showStatusBar() {
    console.warn('html5 is not support showStatusBar')
  }

  hideStatusBar() {
    console.warn('html5 is not support hideStatusBar')
  }
}

import OneKit from "../OneKit.js"
import NodeRef from "./NodeRef.js"
export default class SelectorQuery {
  constructor(wx) {
    this.wx = wx
    this._component = null
    this._defaultComponent = OneKit.current()
    this._queue = []
    this._queueCb = []
  }
  exec(callback) {
    function query(node, selector, single) {
      const result = []
      if (selector.startsWith("#")) {
        if (node.id && `#${node.id}` == selector) {
          if (single) {
            return node
          } else {
            result.push(node)
          }
        }
      } else if (selector.startsWith(".")) {
        if (node.class && node.class.split(" ").includes(selector.substr(1))) {
          if (single) {
            return node
          } else {
            result.push(node)
          }
        }
      }
      for (const child of node.children) {
        const temp = query(child, selector, single)
        if (temp) {
          if (single) {
            return temp
          } else {
            result = result.concat(temp)
          }
        }
      }
      return result
    }
    const results = [];
    for (let i = 0; i < this._queue.length; i++) {
      const item = this._queue[i]
      let node
      if (item.component === 0) {
        node = OneKit.current().root
      } else {
        const parent = item.component || OneKit.current().root
        node = query(parent, item.selector, item.single)
      }

      const result = {}
      for (const field of Object.keys(item.fields)) {
        if (!item.fields[field]) {
          continue
        }

        const rect = {left:0,top:0,width:0,height:0}//node.boundingClientRect
        switch (field) {
          case "id":
            result.id = node.id;
            break
          case "dataset":
            result.dataset = node.dataset;
            break
          case "rect":
            result.left = rect.left
            result.right = rect.right
            result.top = rect.top - 40
            result.bottom = rect.bottom - 40
            break
          case "size":
            result.width = rect.width
            result.height = rect.height
            break
          case "node":
            // result.node =  window.onekit_nodes[`_${node.id}`];
            result.node = node
            break
          case "context":
            switch (node.className.split(" ").filter((name) => {
              return name.startsWith("onekit-")
            })[0]) {
              case "onekit-video":
                result.context = this.wx.createVideoContext(node.id)
                break
              case "onekit-canvas":
                result.context = this.wx.createCanvasContext(node.id)
                break
              case "onekit-liveplayer":
                result.context = this.wx.createLivePlayerContext(node.id)
                break
              case "onekit-editor":
                result.context = this.wx.createEditorContext(node.id)
                break
              case "onekit-map":
                result.context = this.wx.createMapContext(node.id)
                break
              default:
                throw new Error(node.className)
            }
            break
          case "scrollOffset":
            result.scrollTop = node.scrollTop
            result.scrollLeft = node.scrollLeft
            result.scrollWidth = node.scrollWidth
            result.scrollHeight = node.scrollHeight
            break
          default:
            throw new Error(field)
        }
      }
      results.push(result)
      if (this._queueCb[i]) {
        this._queueCb[i](result)
      }
    }
    if (callback && this._queue.length > 0) {
      callback(results)
    }
  } in (component) {
    this._component = component
    return this
  }
  select(selector) {
    return new NodeRef(this, selector, true, this._component)
  }
  selectAll(selector) {
    return new NodeRef(this, selector, false, this._component)
  }
  selectViewport() {
    return new NodeRef(this, "", true, 0)
  }
}

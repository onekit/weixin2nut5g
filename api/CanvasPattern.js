import Image from "./Image"
export default class CanvasPattern {
  constructor(webCanvasContext, image, repetittion) {
    const onekit_image = new Image()
     onekit_image.load(image)
    this.webCanvasContext= webCanvasContext.createPattern(onekit_image, repetittion)
  }
}

import nodejs_thekit from 'nodejs-thekit'
const { PROMISE } = nodejs_thekit
export default class CameraFrameListener {
  constructor() {}
  start(wx_object) {
    const { success, fail, complete } = wx_object

    PROMISE(SUCCESS => {
      SUCCESS()
    }, success, fail, complete)
  }
  stop(wx_object) {
    const { success, fail, complete } = wx_object
    PROMISE(SUCCESS => {
      SUCCESS()
    }, success, fail, complete)
  }
}

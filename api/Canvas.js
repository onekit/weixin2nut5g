import CanvasContext from './CanvasContext'
export default class Canvas {
  cancelAnimationFrame() {}
  createImage() {}
  createImageData() {}
  createPath2D() {}
  getContext() {
    return new CanvasContext()
  }
  requestAnimationFrame() {}
  toDataURL() {}
}
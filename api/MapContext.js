/* eslint-disable max-len */
/* eslint-disable camelcase */
/* eslint-disable no-console */


export default class MapContext {
  constructor(id) {
    this.vMap = window.onekit_nodes[`_${id}`]
  }

  addCustomLayer(object) {
    this.vMap.addCustomLayer_(object)
  }

  addGroundOverlay(object) {
    this.vMap.addGroundOverlay_(object)
  }

  addMarkers(object) {
    this.vMap.addMarkers_(object)
  }

  fromScreenLocation(object) {
    this.vMap.fromScreenLocation_(object)
  }

  getCenterLocation(object) {
    this.vMap.getCenterLocation_(object)
  }

  getRegion(object) {
    this.vMap.getRegion_(object)
  }

  getRotate(object) {
    this.vMap.getRotate_(object)
  }

  getScale(object) {
    this.vMap.getScale_(object)
  }

  getSkew(object) {
    this.vMap.getSkew_(object)
  }

  includePoints(object) {
    this.vMap.includePoints_(object)
  }

  initMarkerCluster(object) {
    this.vMap.initMarkerCluster_(object)
  }

  moveAlong(object) {
    this.vMap.moveAlong_(object)
  }

  moveToLocation(object) {
    this.vMap.moveToLocation_(object)
  }

  on(eventName, handlers) {
    this.vMap.on_(eventName, handlers)
  }

  openMapApp(object) {
    this.vMap.openMapApp_(object)
  }

  removeCustomLayer(object) {
    this.vMap.removeCustomLayer_(object)
  }

  removeGroundOverlay(object) {
    this.vMap.removeGroundOverlay_(object)
  }

  removeMarkers(object) {
    this.vMap.removeMarkers_(object)
  }

  setCenterOffset(object) {

    this.vMap.setCenterOffset_(object)
  }

  toScreenLocation(object) {
    this.vMap.toScreenLocation_(object)
  }

  translateMarker(object) {

    this.vMap.translateMarker_(object)
  }

  updateGroundOverlay(object) {

    this.vMap.updateGroundOverlay_(object)
  }
}

  import Gradient from "./Gradient.js"
  export default class CircularGradient extends Gradient {
    constructor(x, y, r) {
      super();

      this.info = [
        x, y, r
      ];

    }

    toString() {
      return `type:'Circular',points:${JSON.stringify(this.info)},stops:${JSON.stringify(this.colorStops)}`;
    }
  }

export default class BLEPeripheralServer {
  constructor() {}

  addService() {
    console.warn('html5 is not support BLEP!')
  }

  close() {
    console.warn('html5 is not support BLEP!')
  }

  offCharacteristicReadRequest() {
    console.warn('html5 is not support BLEP!')
  }

  offCharacteristicSubscribed() {
    console.warn('html5 is not support BLEP!')
  }

  offCharacteristicUnsubscribed() {
    console.warn('html5 is not support BLEP!')
  }

  offCharacteristicWriteRequest() {
    console.warn('html5 is not support BLEP!')
  }

  onCharacteristicReadRequest() {
    console.warn('html5 is not support BLEP!')
  }

  onCharacteristicSubscribed() {
    console.warn('html5 is not support BLEP!')
  }

  onCharacteristicUnsubscribed() {
    console.warn('html5 is not support BLEP!')
  }

  onCharacteristicWriteRequest() {
    console.warn('html5 is not support BLEP!')
  }
  
  removeService() {
    console.warn('html5 is not support BLEP!')
  }

  startAdvertising() {
    console.warn('html5 is not support BLEP!')
  }

  stopAdvertising() {
    console.warn('html5 is not support BLEP!')
  }
  
  writeCharacteristicValue() {
    console.warn('html5 is not support BLEP!')
  }
}
export default class Danmus {
    constructor() {
        const viedo = document.getElementsByTagName('video')[0]
        this.oBox = viedo.parentElement
        this.cW = this.oBox.offsetWidth
        this.cH = this.oBox.offsetHeight
    }
    random(start, end) {
        return start + ~~(Math.random() * (end - start))
    }
    send(text, color) {
        this.createEle(text, color)
    }
    roll(opt) {
        opt.timing = opt.timing || 'linear'
        opt.color = opt.color || '#fff'
        opt.top = opt.top || 0;
        opt.fontSize = opt.fontSize || 16;
        this._left = parseInt(this.offsetLeft)
        this.style.color = opt.color
        this.style.top = opt.top + 'px'
        this.style.fontSize = opt.fontSize + 'px'
        this.style.whiteSpace = 'nowrap'
        this.timer = setInterval(function () {
            if (this._left <= 10) {
                clearInterval(this.timer)
                this.parentNode.removeChild(this)
                return
            }
            this._left += -2
            this.style.left = this._left + 'px'
        }.bind(this), 1000 / 60)
    }

    createEle(txt, color) {
        const oMessage = document.createElement('span')
        oMessage.innerHTML = txt
        oMessage.style.left = this.cW + 'px'
        oMessage.style.padding = 3 + 'px'
        this.oBox.appendChild(oMessage)
        this.roll.call(oMessage, {
            timing: ['linear', 'ease-out'][~~(Math.random() * 2)],
            color: color || '#' + (~~(Math.random() * (1 << 24))).toString(16),
            top: this.random(0, this.cH),
            fontSize: '15px'
        })
    }
}
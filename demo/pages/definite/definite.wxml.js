import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "definite";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "definite_h";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.bindTap = "check";
  node_0_0_0.dataset.index = "0";
  node_0_0_0.className = VUE.render(data, "definite_h_s definite_h_l {{type == 0 ? 'definite_h_active' : ''}}");
  node_0_0_0.data = data;
  node_0_0_0.keys = ["type"];

  const node_0_0_0_0 = new ui.literal(context, "全部");
  node_0_0_0.appendChild(node_0_0_0_0);

  node_0_0.appendChild(node_0_0_0);

  const node_0_0_1 = new ui.view(context);
  node_0_0_1.data = data;
  node_0_0_1.bindTap = "check";
  node_0_0_1.dataset.index = "1";
  node_0_0_1.className = VUE.render(data, "definite_h_s definite_h_l {{type == 1 ? 'definite_h_active' : ''}}");

  const node_0_0_1_0 = new ui.literal(context, "收到");
  node_0_0_1.appendChild(node_0_0_1_0);

  node_0_0.appendChild(node_0_0_1);

  const node_0_0_2 = new ui.view(context);
  node_0_0_2.data = data;
  node_0_0_2.bindTap = "check";
  node_0_0_2.dataset.index = "2";
  node_0_0_2.className = VUE.render(data, "definite_h_s definite_h_l {{type == 2 ? 'definite_h_active' : ''}}");

  const node_0_0_2_0 = new ui.literal(context, "支出");
  node_0_0_2.appendChild(node_0_0_2_0);

  node_0_0.appendChild(node_0_0_2);

  const node_0_0_3 = new ui.picker(context);
  node_0_0_3.data = data;
  node_0_0_3.className = "definite_h_l definite_h_other";
  node_0_0_3.mode = "date";
  node_0_0_3.fields = "day";
  node_0_0_3.value = VUE.render(data, "{{endTime}}");
  node_0_0_3.start = "2020-1-1";
  node_0_0_3.end = VUE.render(data, "{{endTime}}");
  node_0_0_3.bindChange = "getDateTime";
  node_0_0_3.data = data;
  node_0_0_3.keys = ["endTime"];

  const node_0_0_3_0 = new ui.literal(context, "\n            筛选时间\n            ");
  node_0_0_3.appendChild(node_0_0_3_0);

  const node_0_0_3_1 = new ui.image(context);
  node_0_0_3_1.data = data;
  node_0_0_3_1.mode = "widthFix";
  node_0_0_3_1.src = "../../images/hs11.png";
  node_0_0_3.appendChild(node_0_0_3_1);

  node_0_0.appendChild(node_0_0_3);

  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.view(context);
  node_0_1.data = data;
  node_0_1.className = "list";

  VUE.For(this, data, "datalist", "index", "index", "item", node_0_1, (data, node_0_1) => {
    const node_0_1_0 = new ui.view(context);
    node_0_1_0.data = data;
    node_0_1_0.className = "list_l";

    const node_0_1_0_0 = new ui.view(context);
    node_0_1_0_0.data = data;
    node_0_1_0_0.className = "list_l_left";

    const node_0_1_0_0_0 = new ui.view(context);
    node_0_1_0_0_0.data = data;
    node_0_1_0_0_0.className = "list_l_left_title";

    const node_0_1_0_0_0_0 = new ui.literal(context, VUE.render(data, "{{item.remark}}"));
    node_0_1_0_0_0_0.data = data;
    node_0_1_0_0_0_0.keys = ["item.remark"];
    node_0_1_0_0_0.appendChild(node_0_1_0_0_0_0);

    node_0_1_0_0.appendChild(node_0_1_0_0_0);

    const node_0_1_0_0_1 = new ui.view(context);
    node_0_1_0_0_1.data = data;
    node_0_1_0_0_1.className = "list_l_left_time";

    const node_0_1_0_0_1_0 = new ui.literal(context, VUE.render(data, "{{item.add_time}}"));
    node_0_1_0_0_1_0.data = data;
    node_0_1_0_0_1_0.keys = ["item.add_time"];
    node_0_1_0_0_1.appendChild(node_0_1_0_0_1_0);

    node_0_1_0_0.appendChild(node_0_1_0_0_1);

    node_0_1_0.appendChild(node_0_1_0_0);

    const node_0_1_0_1 = new ui.view(context);
    node_0_1_0_1.data = data;
    node_0_1_0_1.className = "list_l_right";

    const node_0_1_0_1_0 = new ui.view(context);
    node_0_1_0_1_0.data = data;
    node_0_1_0_1_0.className = "add";
    node_0_1_0_1_0.hidden = VUE.render(data, "{{item.type != 1}}");
    node_0_1_0_1_0.data = data;
    node_0_1_0_1_0.keys = ["item.type"];

    const node_0_1_0_1_0_0 = new ui.literal(context, VUE.render(data, "+{{item.amount}}"));
    node_0_1_0_1_0_0.data = data;
    node_0_1_0_1_0_0.keys = ["item.amount"];
    node_0_1_0_1_0.appendChild(node_0_1_0_1_0_0);

    node_0_1_0_1.appendChild(node_0_1_0_1_0);

    const node_0_1_0_1_1 = new ui.view(context);
    node_0_1_0_1_1.data = data;
    node_0_1_0_1_1.className = "del";
    node_0_1_0_1_1.hidden = VUE.render(data, "{{item.type == 1}}");

    const node_0_1_0_1_1_0 = new ui.literal(context, VUE.render(data, "-{{item.amount}}"));
    node_0_1_0_1_1.appendChild(node_0_1_0_1_1_0);

    node_0_1_0_1.appendChild(node_0_1_0_1_1);

    node_0_1_0.appendChild(node_0_1_0_1);

    node_0_1.appendChild(node_0_1_0);
  });

  node_0.appendChild(node_0_1);

  const node_0_2 = new ui.view(context);
  node_0_2.data = data;
  node_0_2.className = "none";
  node_0_2.hidden = VUE.render(data, "{{datalist.length != 0}}");
  node_0_2.data = data;
  node_0_2.keys = ["datalis"];

  const node_0_2_0 = new ui.literal(context, "暂无数据");
  node_0_2.appendChild(node_0_2_0);

  node_0.appendChild(node_0_2);

  node.appendChild(node_0);
}

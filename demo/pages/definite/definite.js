import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  BillLogs
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    datalist: [],
    page: 1,
    endTime: '',
    timestamp: '',
    isPage: true,
    type: 0
  },
  onLoad(options) {
    var timestamp = Date.parse(new Date());
    console.log(timestamp / 1000);
    var date = new Date(timestamp);
    var Y = date.getFullYear();
    var M = date.getMonth() + 1;
    var D = date.getDate();
    console.log(((((((Y + '-')) + M)) + '-')) + D);
    this.setData({
      endTime: ((((((Y + '-')) + M)) + '-')) + D,
      timestamp: timestamp / 1000
    }, () => {
      this.getList()
    });
  },
  getList(pan) {
    BillLogs({
      type: this.data.type,
      page: this.data.page,
      num: 10,
      end_time: this.data.timestamp
    }, (data) => {
      wx.stopPullDownRefresh();
      if (data.code == 1) {
        this.setData({
          isPage: data.data.length == 10 ? true : false
        });
        if (!pan) {
          this.setData({
            datalist: data.data
          });
        } else {
          var arr = this.data.datalist;
          this.setData({
            datalist: arr.concat(data.data)
          });
        }
      };
    });
  },
  getDateTime(e) {
    this.setData({
      timestamp: ((((Date.parse(new Date(e.detail.value)) / 1000)) + ((((60 * 60)) * 16)))) - 1,
      page: 1
    }, () => {
      this.getList()
    });
  },
  check(e) {
    console.log(e);
    this.setData({
      type: e.currentTarget.dataset.index,
      page: 1
    }, () => {
      this.getList()
    });
  },
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {
    this.setData({
      page: 1
    });
    this.getList();
  },
  onReachBottom() {
    if (this.data.isPage) {
      this.setData({
        page: this.data.page + 1
      });
      this.getList(1);
    };
  },
  onShareAppMessage() {}
});

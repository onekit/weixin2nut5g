import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "pushMoney";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "pushMoney_box";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.className = "pushMoney_box_mount";

  const node_0_0_0_0 = new ui.literal(context, "提现金额");
  node_0_0_0.appendChild(node_0_0_0_0);

  node_0_0.appendChild(node_0_0_0);

  const node_0_0_1 = new ui.view(context);
  node_0_0_1.data = data;
  node_0_0_1.className = "pushMoney_box_input";

  const node_0_0_1_0 = new ui.literal(context, "\n            ¥\n            ");
  node_0_0_1.appendChild(node_0_0_1_0);

  const node_0_0_1_1 = new ui.input(context);
  node_0_0_1_1.data = data;
  node_0_0_1_1.focus = VUE.render(data, "{{true}}");
  node_0_0_1_1.bindInput = "setNumber";
  node_0_0_1_1.value = VUE.render(data, "{{price}}");
  node_0_0_1_1.type = "digit";
  node_0_0_1_1.data = data;
  node_0_0_1_1.keys = ["true", "price"];
  node_0_0_1.appendChild(node_0_0_1_1);

  node_0_0.appendChild(node_0_0_1);

  const node_0_0_2 = new ui.view(context);
  node_0_0_2.data = data;
  node_0_0_2.className = "pushMoney_box_all";

  const node_0_0_2_0 = new ui.literal(context, VUE.render(data, "钱包余额 ¥{{money}}，"));
  node_0_0_2_0.data = data;
  node_0_0_2_0.keys = ["money"];
  node_0_0_2.appendChild(node_0_0_2_0);

  const node_0_0_2_1 = new ui.text(context);
  node_0_0_2_1.data = data;
  node_0_0_2_1.bindTap = "tixianAll";

  const node_0_0_2_1_0 = new ui.literal(context, "全部提现");
  node_0_0_2_1.appendChild(node_0_0_2_1_0);

  node_0_0_2.appendChild(node_0_0_2_1);

  node_0_0.appendChild(node_0_0_2);

  const node_0_0_3 = new ui.button(context);
  node_0_0_3.data = data;
  node_0_0_3.bindTap = "insetMoney";
  node_0_0_3.className = "insetMoney";

  const node_0_0_3_0 = new ui.literal(context, "提现");
  node_0_0_3.appendChild(node_0_0_3_0);

  node_0_0.appendChild(node_0_0_3);

  node_0.appendChild(node_0_0);

  node.appendChild(node_0);
}

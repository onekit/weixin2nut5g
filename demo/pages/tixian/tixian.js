import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  extract
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    price: '',
    money: '0.00'
  },
  onLoad(options) {
    this.setData({
      money: options.money
    });
  },
  onReady() {},
  setNumber(e) {
    this.setData({
      price: e.detail.value
    });
  },
  tixianAll() {
    this.setData({
      price: this.data.money
    });
  },
  insetMoney() {
    if (this.data.price < 0.3) {
      app.globalData.toast('提现金额不能低于0.3');
      return;
    };
    if (this.data.price > 5000) {
      app.globalData.toast('单次提现金额不能高于5000');
      return;
    };
    if (this.data.price) {
      extract({
        amount: this.data.price,
        openid: wx.getStorageSync('openid')
      }, (data) => {
        if (data.code == 1) {
          app.globalData.toast('提现成功', 'success');
          setTimeout(() => {
            wx.navigateBack({
              delta: 1
            })
          }, 800);
        } else {
          app.globalData.toast(data.msg);
        }
      });
    } else {
      app.globalData.toast('请输入金额');
    };
  },
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  wxLogin,
  code2Session
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    packet_id: ''
  },
  onLoad(options) {
    console.log(options);
    if (options.packet_id) {
      this.setData({
        packet_id: options.packet_id
      });
    };
  },
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onGotUserInfo(e) {
    console.log(e);
    var _this = this;
    if (e.detail.errMsg == 'getUserInfo:ok') {
      app.globalData.loading('加载中...');
      wx.login({
        success(res) {
          if (res.code) {
            wx.getUserInfo({
              success(res1) {
                wxLogin({
                  code: res.code,
                  rawData: res1.rawData,
                  signature: res1.signature,
                  encryptedData: res1.encryptedData,
                  iv: res1.iv
                }, (data) => {
                  wx.hideLoading();
                  if (data.code == 1) {
                    app.globalData.toast(data.msg, 'success');
                    wx.setStorageSync('token', data.data.token);
                    if (wx.getStorageSync('isqiye') == 1) {
                      wx.qy.login({
                        success(res) {
                          if (res.code) {
                            code2Session({
                              code: res.code
                            }, (data) => {
                              if (data.errcode == 0) {
                                wx.setStorageSync('corpid', data.corpid);
                                wx.setStorageSync('userid', data.userid);
                                const pages = getCurrentPages();
                                const beforePage = pages[pages.length - 2];
                                wx.navigateBack({
                                  delta: 1,
                                  success() {
                                    if (_this.data.packet_id) {
                                      beforePage.onLoad({
                                        packet_id: _this.data.packet_id
                                      });
                                    };
                                  }
                                });
                              }
                            });
                          };
                        }
                      });
                    } else {
                      const pages = getCurrentPages();
                      const beforePage = pages[pages.length - 2];
                      wx.navigateBack({
                        delta: 1,
                        success() {
                          if (_this.data.packet_id) {
                            beforePage.onLoad({
                              packet_id: _this.data.packet_id
                            });
                          };
                        }
                      });
                    }
                  } else {
                    app.globalData.toast(data.msg, 'none');
                  };
                });
              }
            });
          };
        }
      });
    };
  },
  back() {
    wx.navigateBack({
      delta: 1
    });
  },
  onShareAppMessage() {}
});

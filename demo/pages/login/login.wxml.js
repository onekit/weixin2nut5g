import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "login";

  const node_0_0 = new ui.image(context);
  node_0_0.data = data;
  node_0_0.src = "../../images/logo.png";
  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.button(context);
  node_0_1.data = data;
  node_0_1.hoverClass = "none";
  node_0_1.bindGetuserinfo = "onGotUserInfo";
  node_0_1.openType = "getUserInfo";

  const node_0_1_0 = new ui.literal(context, "登录");
  node_0_1.appendChild(node_0_1_0);

  node_0.appendChild(node_0_1);

  const node_0_2 = new ui.button(context);
  node_0_2.data = data;
  node_0_2.bindTap = "back";
  node_0_2.hoverClass = "none";
  node_0_2.className = "getUserInfo1";

  const node_0_2_0 = new ui.literal(context, "暂不登录");
  node_0_2.appendChild(node_0_2_0);

  node_0.appendChild(node_0_2);

  node.appendChild(node_0);
}

import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "detail";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "info_back_ul";

  VUE.For(this, data, "list", "index", "index", "item", node_0_0, (data, node_0_0) => {
    const node_0_0_0 = new ui.view(context);
    node_0_0_0.data = data;
    node_0_0_0.className = "info_back_li";

    const node_0_0_0_0 = new ui.image(context);
    node_0_0_0_0.data = data;
    node_0_0_0_0.className = "info_back_li_left";
    node_0_0_0_0.src = VUE.render(data, "{{item.avatarurl}}");
    node_0_0_0_0.data = data;
    node_0_0_0_0.keys = ["item.avatarurl"];
    node_0_0_0.appendChild(node_0_0_0_0);

    const node_0_0_0_1 = new ui.view(context);
    node_0_0_0_1.data = data;
    node_0_0_0_1.className = "info_back_li_content";

    const node_0_0_0_1_0 = new ui.literal(context, VUE.render(data, "\n                {{item.nickname}}\n            "));
    node_0_0_0_1_0.data = data;
    node_0_0_0_1_0.keys = ["item.nickname"];
    node_0_0_0_1.appendChild(node_0_0_0_1_0);

    node_0_0_0.appendChild(node_0_0_0_1);

    const node_0_0_0_2 = new ui.view(context);
    node_0_0_0_2.data = data;
    node_0_0_0_2.className = "info_back_li_right";

    const node_0_0_0_2_0 = new ui.view(context);
    node_0_0_0_2_0.data = data;
    node_0_0_0_2_0.className = "info_back_li_price_l";

    const node_0_0_0_2_0_0 = new ui.literal(context, VUE.render(data, "+{{item.amount}}"));
    node_0_0_0_2_0_0.data = data;
    node_0_0_0_2_0_0.keys = ["item.amount"];
    node_0_0_0_2_0.appendChild(node_0_0_0_2_0_0);

    node_0_0_0_2.appendChild(node_0_0_0_2_0);

    const node_0_0_0_2_1 = new ui.view(context);
    node_0_0_0_2_1.data = data;
    node_0_0_0_2_1.className = "info_back_li_price_r";

    const node_0_0_0_2_1_0 = new ui.literal(context, VUE.render(data, "{{item.add_time}}"));
    node_0_0_0_2_1_0.data = data;
    node_0_0_0_2_1_0.keys = ["item.add_time"];
    node_0_0_0_2_1.appendChild(node_0_0_0_2_1_0);

    node_0_0_0_2.appendChild(node_0_0_0_2_1);

    node_0_0_0.appendChild(node_0_0_0_2);

    node_0_0.appendChild(node_0_0_0);
  });

  const node_0_0_1 = new ui.view(context);
  node_0_0_1.data = data;
  node_0_0_1.hidden = VUE.render(data, "{{list.length != 0}}");
  node_0_0_1.className = "info_back_none";
  node_0_0_1.data = data;
  node_0_0_1.keys = ["lis"];

  const node_0_0_1_0 = new ui.view(context);
  node_0_0_1_0.data = data;

  const node_0_0_1_0_0 = new ui.literal(context, "暂无领取记录");
  node_0_0_1_0.appendChild(node_0_0_1_0_0);

  node_0_0_1.appendChild(node_0_0_1_0);

  node_0_0.appendChild(node_0_0_1);

  node_0.appendChild(node_0_0);

  node.appendChild(node_0);
}

import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
export default OnekitPage({
  data: {
    list: []
  },
  onLoad(options) {
    this.setData({
      list: JSON.parse(options.data).getlist ? JSON.parse(options.data).getlist : []
    });
  },
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "main";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "title";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.className = "title_left";

  const node_0_0_0_0 = new ui.literal(context, "我的钱包");
  node_0_0_0.appendChild(node_0_0_0_0);

  node_0_0.appendChild(node_0_0_0);

  const node_0_0_1 = new ui.navigator(context);
  node_0_0_1.data = data;
  node_0_0_1.hidden = VUE.render(data, "{{!istt}}");
  node_0_0_1.url = "/pages/definite/definite";
  node_0_0_1.className = "title_right";
  node_0_0_1.data = data;
  node_0_0_1.keys = ["istt"];

  const node_0_0_1_0 = new ui.literal(context, "明细");
  node_0_0_1.appendChild(node_0_0_1_0);

  node_0_0.appendChild(node_0_0_1);

  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.navigator(context);
  node_0_1.data = data;
  node_0_1.hidden = VUE.render(data, "{{!istt}}");
  node_0_1.url = "/pages/wallet/wallet";
  node_0_1.className = "tixian";

  const node_0_1_0 = new ui.view(context);
  node_0_1_0.data = data;
  node_0_1_0.className = "tixian_left";

  const node_0_1_0_0 = new ui.image(context);
  node_0_1_0_0.data = data;
  node_0_1_0_0.src = "../../images/hs2.png";
  node_0_1_0.appendChild(node_0_1_0_0);

  const node_0_1_0_1 = new ui.literal(context, VUE.render(data, "\n            {{balance}}\n        "));
  node_0_1_0_1.data = data;
  node_0_1_0_1.keys = ["balance"];
  node_0_1_0.appendChild(node_0_1_0_1);

  node_0_1.appendChild(node_0_1_0);

  const node_0_1_1 = new ui.image(context);
  node_0_1_1.data = data;
  node_0_1_1.className = "more";
  node_0_1_1.src = "../../images/hs10.png";
  node_0_1.appendChild(node_0_1_1);

  node_0.appendChild(node_0_1);

  const node_0_2 = new ui.navigator(context);
  node_0_2.data = data;
  node_0_2.hidden = VUE.render(data, "{{!istt}}");
  node_0_2.url = "/pages/info/info";
  node_0_2.className = "tixian tixian1";

  const node_0_2_0 = new ui.view(context);
  node_0_2_0.data = data;
  node_0_2_0.className = "tixian_left";

  const node_0_2_0_0 = new ui.image(context);
  node_0_2_0_0.data = data;
  node_0_2_0_0.src = "../../images/hs17.png";
  node_0_2_0.appendChild(node_0_2_0_0);

  const node_0_2_0_1 = new ui.literal(context, "\n            红包记录\n        ");
  node_0_2_0.appendChild(node_0_2_0_1);

  node_0_2.appendChild(node_0_2_0);

  const node_0_2_1 = new ui.image(context);
  node_0_2_1.data = data;
  node_0_2_1.className = "more";
  node_0_2_1.src = "../../images/hs4.png";
  node_0_2.appendChild(node_0_2_1);

  node_0.appendChild(node_0_2);

  const node_0_3 = new ui.view(context);
  node_0_3.data = data;
  node_0_3.bindTap = "logout";
  node_0_3.className = "tixian tixian1";

  const node_0_3_0 = new ui.view(context);
  node_0_3_0.data = data;
  node_0_3_0.className = "tixian_left";

  const node_0_3_0_0 = new ui.image(context);
  node_0_3_0_0.data = data;
  node_0_3_0_0.src = "../../images/hs3.png";
  node_0_3_0.appendChild(node_0_3_0_0);

  const node_0_3_0_1 = new ui.literal(context, "\n            退出登录\n        ");
  node_0_3_0.appendChild(node_0_3_0_1);

  node_0_3.appendChild(node_0_3_0);

  const node_0_3_1 = new ui.image(context);
  node_0_3_1.data = data;
  node_0_3_1.className = "more";
  node_0_3_1.src = "../../images/hs4.png";
  node_0_3.appendChild(node_0_3_1);

  node_0.appendChild(node_0_3);

  node.appendChild(node_0);
}

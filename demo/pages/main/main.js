import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  getUserInfo
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    balance: ' 0.00',
    istt: false
  },
  onLoad(options) {},
  onReady() {},
  onShow() {
    if (((Date.parse(new Date()) / 1000)) > 1597226400) {
      this.setData({
        istt: true
      });
      getUserInfo({}, (data) => {
        if (data.code == 0) {
          this.setData({
            balance: data.data.balance
          });
          wx.setStorageSync('balance', data.data.balance);
          wx.setStorageSync('openid', data.data.openid);
        }
      });
    };
  },
  logout() {
    wx.removeStorageSync('token');
    wx.removeStorageSync('userid');
    wx.navigateTo({
      url: '/pages/login/login'
    });
  },
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

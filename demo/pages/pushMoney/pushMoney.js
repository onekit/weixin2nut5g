import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  createOrder
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    price: ''
  },
  onLoad(options) {},
  onReady() {},
  setNumber(e) {
    this.setData({
      price: e.detail.value
    });
  },
  insetMoney() {
    if (this.data.price < 0.3) {
      app.globalData.toast('充值金额不能低于0.3');
      return;
    };
    if (this.data.price > 5000) {
      app.globalData.toast('充值金额不能高于5000');
      return;
    };
    if (this.data.price) {
      createOrder({
        total_fee: this.data.price
      }, (data) => {
        if (data.data.code == 1) {
          wx.requestPayment({
            timeStamp: data.data.timeStamp,
            nonceStr: data.data.nonceStr,
            package: data.data.package,
            signType: 'MD5',
            paySign: data.data.paySign,
            success(res) {
              app.globalData.toast('充值成功', 'success');
              wx.navigateBack({
                delta: 1
              });
            },
            fail(res) {
              app.globalData.toast('充值失败');
            }
          });
        } else {
          app.globalData.toast(data.msg);
        }
      });
    } else {
      app.globalData.toast('请输入金额');
    };
  },
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

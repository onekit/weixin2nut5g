import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "info";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "info_back";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.className = "info_back_ul";

  VUE.For(this, data, "datalist", "index", "index", "item", node_0_0_0, (data, node_0_0_0) => {
    const node_0_0_0_0 = new ui.view(context);
    node_0_0_0_0.data = data;
    node_0_0_0_0.bindTap = "detail";
    node_0_0_0_0.dataItem = VUE.render(data, "{{item}}");
    node_0_0_0_0.className = "info_back_li";
    node_0_0_0_0.data = data;
    node_0_0_0_0.keys = ["item"];

    const node_0_0_0_0_0 = new ui.image(context);
    node_0_0_0_0_0.data = data;
    node_0_0_0_0_0.className = "info_back_li_left";
    node_0_0_0_0_0.src = "../../images/hs18.png";
    node_0_0_0_0.appendChild(node_0_0_0_0_0);

    const node_0_0_0_0_1 = new ui.view(context);
    node_0_0_0_0_1.data = data;
    node_0_0_0_0_1.className = "info_back_li_content";

    const node_0_0_0_0_1_0 = new ui.view(context);
    node_0_0_0_0_1_0.data = data;
    node_0_0_0_0_1_0.className = "info_back_li_content_title";

    const node_0_0_0_0_1_0_0 = new ui.literal(context, VUE.render(data, "{{item.type == 1 ? '拼手气红包' : '普通红包'}}"));
    node_0_0_0_0_1_0_0.data = data;
    node_0_0_0_0_1_0_0.keys = ["item.type"];
    node_0_0_0_0_1_0.appendChild(node_0_0_0_0_1_0_0);

    node_0_0_0_0_1.appendChild(node_0_0_0_0_1_0);

    const node_0_0_0_0_1_1 = new ui.view(context);
    node_0_0_0_0_1_1.data = data;
    node_0_0_0_0_1_1.className = "info_back_li_content_total";

    const node_0_0_0_0_1_1_0 = new ui.literal(context, VUE.render(data, "共{{item.total_amount}}元，已领 {{item.num-item.sur_num}}/{{item.num}} 个"));
    node_0_0_0_0_1_1_0.data = data;
    node_0_0_0_0_1_1_0.keys = ["item.total_amount", "item.num", "item.sur_num"];
    node_0_0_0_0_1_1.appendChild(node_0_0_0_0_1_1_0);

    node_0_0_0_0_1.appendChild(node_0_0_0_0_1_1);

    const node_0_0_0_0_1_2 = new ui.view(context);
    node_0_0_0_0_1_2.data = data;
    node_0_0_0_0_1_2.className = "info_back_li_content_time";

    const node_0_0_0_0_1_2_0 = new ui.literal(context, VUE.render(data, "{{item.add_time}}"));
    node_0_0_0_0_1_2_0.data = data;
    node_0_0_0_0_1_2_0.keys = ["item.add_time"];
    node_0_0_0_0_1_2.appendChild(node_0_0_0_0_1_2_0);

    node_0_0_0_0_1.appendChild(node_0_0_0_0_1_2);

    node_0_0_0_0.appendChild(node_0_0_0_0_1);

    const node_0_0_0_0_2 = new ui.navigator(context);
    node_0_0_0_0_2.data = data;
    node_0_0_0_0_2.catchTap = "catTp";
    node_0_0_0_0_2.url = VUE.render(data, "/pages/share/share?packet_id={{item.packet_id}}");
    node_0_0_0_0_2.data = data;
    node_0_0_0_0_2.keys = ["item.packet_id"];

    const node_0_0_0_0_2_0 = new ui.image(context);
    node_0_0_0_0_2_0.data = data;
    node_0_0_0_0_2_0.className = "info_back_li_right";
    node_0_0_0_0_2_0.src = "../../images/hs19.png";
    node_0_0_0_0_2.appendChild(node_0_0_0_0_2_0);

    node_0_0_0_0.appendChild(node_0_0_0_0_2);

    node_0_0_0.appendChild(node_0_0_0_0);
  });

  const node_0_0_0_1 = new ui.view(context);
  node_0_0_0_1.data = data;
  node_0_0_0_1.hidden = VUE.render(data, "{{datalist.length != 0}}");
  node_0_0_0_1.className = "info_back_none";
  node_0_0_0_1.data = data;
  node_0_0_0_1.keys = ["datalis"];

  const node_0_0_0_1_0 = new ui.image(context);
  node_0_0_0_1_0.data = data;
  node_0_0_0_1_0.mode = "widthFix";
  node_0_0_0_1_0.src = "../../images/hs20.png";
  node_0_0_0_1.appendChild(node_0_0_0_1_0);

  const node_0_0_0_1_1 = new ui.view(context);
  node_0_0_0_1_1.data = data;

  const node_0_0_0_1_1_0 = new ui.literal(context, "暂无记录");
  node_0_0_0_1_1.appendChild(node_0_0_0_1_1_0);

  node_0_0_0_1.appendChild(node_0_0_0_1_1);

  node_0_0_0.appendChild(node_0_0_0_1);

  node_0_0.appendChild(node_0_0_0);

  node_0.appendChild(node_0_0);

  node.appendChild(node_0);
}

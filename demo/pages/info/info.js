import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  redPacketList
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    page: 1,
    isPage: true,
    datalist: []
  },
  onLoad(options) {
    this.getList();
  },
  getList(pan) {
    redPacketList({
      page: this.data.page,
      num: 10
    }, (data) => {
      wx.stopPullDownRefresh();
      if (data.code == 1) {
        this.setData({
          isPage: data.data.length == 10 ? true : false
        });
        if (!pan) {
          this.setData({
            datalist: data.data
          });
        } else {
          var arr = this.data.datalist;
          this.setData({
            datalist: arr.concat(data.data)
          });
        }
      };
    });
  },
  detail(item) {
    wx.navigateTo({
      url: '/pages/detail/detail?data=' + JSON.stringify(item.currentTarget.dataset.item)
    });
  },
  catTp() {},
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {
    this.setData({
      page: 1
    });
    this.getList();
  },
  onReachBottom() {
    if (this.data.isPage) {
      this.setData({
        page: this.data.page + 1
      });
      this.getList(1);
    };
  },
  onShareAppMessage() {}
});

export default [{
  "type": "Selector",
  "value": ".award",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " rgb(241, 241, 241)",
      "key": "background-color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 40rpx 50rpx",
      "key": "padding"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " border-box",
      "key": "box-sizing"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-type",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 70rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " flex",
      "key": "display"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "justify-content"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 40rpx",
      "key": "margin-bottom"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-type-l",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 50%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "text-align"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 70rpx",
      "key": "line-height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 30rpx",
      "key": "font-size"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 4rpx",
      "key": "border-radius"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #fff",
      "key": "background-color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #999",
      "key": "color"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".isActive",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #D23C3D",
      "key": "background-color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #fff",
      "key": "color"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 120rpx",
      "key": "margin-bottom"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #fff",
      "key": "background-color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 11rpx",
      "key": "border-radius"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " flex",
      "key": "display"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "align-items"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 112rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 112rpx",
      "key": "line-height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #0A0A0A",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 36rpx",
      "key": "font-size"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 0 20rpx",
      "key": "padding"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " border-box",
      "key": "box-sizing"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 40rpx",
      "key": "margin-bottom"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l-left",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 200rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 36rpx",
      "key": "font-size"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " bold",
      "key": "font-weight"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l-right",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 60rpx",
      "key": "width"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l input",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 1",
      "key": "flex"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " right",
      "key": "text-align"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 70rpx",
      "key": "line-height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 20rpx",
      "key": "padding-right"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " border-box",
      "key": "box-sizing"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l-left image",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 42rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 42rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " translateY(10rpx)",
      "key": "transform"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".tips",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #999999",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 24rpx",
      "key": "font-size"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 20rpx",
      "key": "margin-bottom"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 20rpx",
      "key": "margin-top"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".upload",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 29rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 19rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " right",
      "key": "float"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 50rpx",
      "key": "margin-top"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-l-loga",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " left",
      "key": "float"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #999999",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 90%",
      "key": "width"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": "button",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 366rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": "90rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": "#D23C3D",
      "key": "background"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": "10rpx",
      "key": "border-radius"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #fff",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 36rpx",
      "key": "font-size"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".award-content-r",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " block",
      "key": "display"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " fixed",
      "key": "position"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 0",
      "key": "top"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 0",
      "key": "left"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " rgba(0, 0, 0, .7)",
      "key": "background-color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 10",
      "key": "z-index"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100rpx",
      "key": "padding-top"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "text-align"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_box",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "height"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull .redPacket",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 540rpx",
      "key": "width"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".close",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 76rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " block",
      "key": "display"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 0 auto",
      "key": "margin"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_but",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " absolute",
      "key": "position"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 110rpx",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 110rpx",
      "key": "height"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 264rpx",
      "key": "top"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 310rpx",
      "key": "left"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_info",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " absolute",
      "key": "position"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 170rpx",
      "key": "top"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #825617",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 30rpx",
      "key": "font-size"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "text-align"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_money",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " absolute",
      "key": "position"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": "260rpx",
      "key": "top"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 100%",
      "key": "width"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " center",
      "key": "text-align"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_money .getbull_money_uu",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #d59951",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 90rpx",
      "key": "font-size"
    },
    "children": []
  }]
}, {
  "type": "Selector",
  "value": ".getbull_money .getbull_money_to",
  "children": [{
    "type": "Css",
    "css": {
      "priority": "",
      "value": " #d59951",
      "key": "color"
    },
    "children": []
  }, {
    "type": "Css",
    "css": {
      "priority": "",
      "value": " 36rpx",
      "key": "font-size"
    },
    "children": []
  }]
}]

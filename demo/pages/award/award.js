import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  createRedPacket,
  wxLogin,
  code2Session,
  getPacket,
  getPacketInfo
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    isType: 1,
    array: [
      '全部',
      '男',
      '女'
    ],
    sex: 0,
    total_amount: '',
    once_amount: '',
    num: '',
    isLogin: false,
    isPacket: 0,
    istext: false,
    packet_id: '',
    money: '0.00',
    istt: false
  },
  onLoad(options) {
    console.log(options);
    if (((Date.parse(new Date()) / 1000)) > 1597226400) {
      this.setData({
        istt: true
      });
      if (options.packet_id) {
        getPacketInfo({
          packet_id: options.packet_id
        }, (data) => {
          if (data.code == 1) {
            this.setData({
              isPacket: 1,
              packet_id: options.packet_id
            });
          }
        });
      }
      if (wx.getStorageSync('token')) {
        this.setData({
          isLogin: true
        });
      }
    };
  },
  openPacket() {
    app.globalData.loading();
    getPacket({
      packet_id: this.data.packet_id
    }, (data) => {
      wx.hideLoading();
      if (data.code == 1) {
        this.setData({
          isPacket: 2,
          money: data.data.amount
        });
        setTimeout(() => {
          this.setData({
            istext: true
          })
        }, 500);
      } else {
        app.globalData.toast(data.msg, 'none');
      };
    });
  },
  bindPickerChange(e) {
    this.setData({
      sex: e.detail.value
    });
  },
  fahong(e) {
    console.log(e);
    if (((this.data.isType == 1) && !this.data.total_amount)) {
      app.globalData.toast('请输入总金额');
      return;
    };
    if (((this.data.isType == 2) && !this.data.once_amount)) {
      app.globalData.toast('请输入单个金额');
      return;
    };
    if (!this.data.num) {
      app.globalData.toast('请输入个数');
      return;
    };
    createRedPacket({
      type: this.data.isType,
      total_amount: this.data.total_amount,
      num: this.data.num,
      once_amount: this.data.once_amount,
      limit_sex: this.data.sex,
      corpid: wx.getStorageSync('corpid')
    }, (data) => {
      if (data.code == 1) {
        app.globalData.toast(data.msg, 'success');
        this.setData({
          sex: 0,
          total_amount: '',
          once_amount: '',
          num: '',
          isLogin: false
        });
        wx.navigateTo({
          url: '/pages/share/share?packet_id=' + data.data.packet_id
        });
      } else {
        app.globalData.toast(data.msg);
      }
    });
  },
  check(e) {
    this.setData({
      isType: e.currentTarget.dataset.index
    });
  },
  setAmount1(e) {

    console.log("setAmount1~~~~~~~~~~~~~", e.detail)
    this.setData({
      once_amount: e.detail.value
    });
  },
  setAmount(e) {
    console.log("setAmount~~~~~~~~~~~~~", e.detail)
    this.setData({
      total_amount: e.detail.value
    });
  },
  closePacket() {
    this.setData({
      isPacket: 0,
      istext: false
    });
  },
  setNumber(e) {
    this.setData({
      num: e.detail.value
    });
  },
  onGotUserInfo(e) {
    console.log(e);
    var _this = this;
    if (e.detail.errMsg == 'getUserInfo:ok') {
      app.globalData.loading('加载中...');
      wx.login({
        success(res) {
          if (res.code) {
            wx.getUserInfo({
              success(res1) {
                wxLogin({
                  code: res.code,
                  rawData: res1.rawData,
                  signature: res1.signature,
                  encryptedData: res1.encryptedData,
                  iv: res1.iv
                }, (data) => {
                  wx.hideLoading();
                  if (data.code == 1) {
                    wx.setStorageSync('token', data.data.token);
                    _this.setData({
                      isLogin: true
                    });
                    if (wx.getStorageSync('isqiye') == 1) {
                      wx.qy.login({
                        success(res) {
                          if (res.code) {
                            code2Session({
                              code: res.code
                            }, (data) => {
                              if (data.errcode == 0) {
                                wx.setStorageSync('corpid', data.corpid);
                                wx.setStorageSync('userid', data.userid);
                                _this.fahong();
                              }
                            });
                          } else {
                            console.log('登录失败！' + res.errMsg);
                          };
                        }
                      });
                    } else {
                      _this.fahong();
                    }
                  } else {
                    app.globalData.toast(data.msg, 'none');
                  };
                });
              }
            });
          };
        }
      });
    };
  },
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

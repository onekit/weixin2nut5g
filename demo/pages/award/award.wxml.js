import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.hidden = VUE.render(data, "{{!istt}}");
  node_0.className = "award";
  node_0.keys = ["istt"];

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "award-type";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.bindTap = "check";
  node_0_0_0.className = VUE.render(data, "award-type-l {{isType == 1 ? 'isActive' : ''}}");
  node_0_0_0.dataset.index = "1";
  node_0_0_0.keys = ["isType"];

  const node_0_0_0_0 = new ui.literal(context, "拼手气红包");
  node_0_0_0.appendChild(node_0_0_0_0);

  node_0_0.appendChild(node_0_0_0);

  const node_0_0_1 = new ui.view(context);
  node_0_0_1.data = data;
  node_0_0_1.bindTap = "check";
  node_0_0_1.className = VUE.render(data, "award-type-l {{isType == 2 ? 'isActive' : ''}}");
  node_0_0_1.dataset.index = "2";
  node_0_0_1.keys = ["isType"];

  const node_0_0_1_0 = new ui.literal(context, "普通红包");
  node_0_0_1.appendChild(node_0_0_1_0);

  node_0_0.appendChild(node_0_0_1);

  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.view(context);
  node_0_1.data = data;
  node_0_1.className = "award-content";

  const node_0_1_0 = new ui.view(context);
  node_0_1_0.data = data;
  node_0_1_0.hidden = VUE.render(data, "{{isType == 2}}");
  node_0_1_0.className = "award-content-l";
  node_0_1_0.keys = ["isType"];

  const node_0_1_0_0 = new ui.view(context);
  node_0_1_0_0.data = data;
  node_0_1_0_0.className = "award-content-l-left";

  const node_0_1_0_0_0 = new ui.literal(context, "\n        总金额\n        ");
  node_0_1_0_0.appendChild(node_0_1_0_0_0);

  const node_0_1_0_0_1 = new ui.image(context);
  node_0_1_0_0_1.data = data;
  node_0_1_0_0_1.src = "../../images/hs1.png";
  node_0_1_0_0.appendChild(node_0_1_0_0_1);

  node_0_1_0.appendChild(node_0_1_0_0);

  const node_0_1_0_1 = new ui.input(context);
  node_0_1_0_1.data = data;
  node_0_1_0_1.bindInput = "setAmount";
  node_0_1_0_1.value = VUE.render(data, "{{total_amount}}");
  node_0_1_0_1.placeholder = "请输入金额";
  node_0_1_0_1.type = "digit";
  node_0_1_0_1.keys = ["total_amount"];
  node_0_1_0.appendChild(node_0_1_0_1);

  const node_0_1_0_2 = new ui.view(context);
  node_0_1_0_2.data = data;
  node_0_1_0_2.className = "award-content-right";

  const node_0_1_0_2_0 = new ui.literal(context, "元");
  node_0_1_0_2.appendChild(node_0_1_0_2_0);

  node_0_1_0.appendChild(node_0_1_0_2);

  node_0_1.appendChild(node_0_1_0);

  const node_0_1_1 = new ui.view(context);
  node_0_1_1.data = data;
  node_0_1_1.hidden = VUE.render(data, "{{isType == 1}}");
  node_0_1_1.className = "award-content-l";
  node_0_1_1.keys = ["isType"];

  const node_0_1_1_0 = new ui.view(context);
  node_0_1_1_0.data = data;
  node_0_1_1_0.className = "award-content-l-left";

  const node_0_1_1_0_0 = new ui.literal(context, "\n        单个金额\n      ");
  node_0_1_1_0.appendChild(node_0_1_1_0_0);

  node_0_1_1.appendChild(node_0_1_1_0);

  const node_0_1_1_1 = new ui.input(context);
  node_0_1_1_1.data = data;
  node_0_1_1_1.bindInput = "setAmount1";
  node_0_1_1_1.value = VUE.render(data, "{{once_amount}}");
  node_0_1_1_1.placeholder = "请输入金额";
  node_0_1_1_1.type = "digit";
  node_0_1_1_1.keys = ["once_amount"];
  node_0_1_1.appendChild(node_0_1_1_1);

  const node_0_1_1_2 = new ui.view(context);
  node_0_1_1_2.data = data;
  node_0_1_1_2.className = "award-content-right";

  const node_0_1_1_2_0 = new ui.literal(context, "元");
  node_0_1_1_2.appendChild(node_0_1_1_2_0);

  node_0_1_1.appendChild(node_0_1_1_2);

  node_0_1.appendChild(node_0_1_1);

  const node_0_1_2 = new ui.view(context);
  node_0_1_2.data = data;
  node_0_1_2.className = "award-content-l";

  const node_0_1_2_0 = new ui.view(context);
  node_0_1_2_0.data = data;
  node_0_1_2_0.className = "award-content-l-left";

  const node_0_1_2_0_0 = new ui.literal(context, "\n        红包个数\n      ");
  node_0_1_2_0.appendChild(node_0_1_2_0_0);

  node_0_1_2.appendChild(node_0_1_2_0);

  const node_0_1_2_1 = new ui.input(context);
  node_0_1_2_1.data = data;
  node_0_1_2_1.style = "color:#999";
  node_0_1_2_1.bindInput = "setNumber";
  node_0_1_2_1.value = VUE.render(data, "{{num}}");
  node_0_1_2_1.placeholder = "请输入个数";
  node_0_1_2_1.type = "number";
  node_0_1_2_1.keys = ["num"];
  node_0_1_2.appendChild(node_0_1_2_1);

  const node_0_1_2_2 = new ui.view(context);
  node_0_1_2_2.data = data;
  node_0_1_2_2.className = "award-content-right";

  const node_0_1_2_2_0 = new ui.literal(context, "个");
  node_0_1_2_2.appendChild(node_0_1_2_2_0);

  node_0_1_2.appendChild(node_0_1_2_2);

  node_0_1.appendChild(node_0_1_2);

  const node_0_1_3 = new ui.view(context);
  node_0_1_3.data = data;
  node_0_1_3.className = "tips";

  const node_0_1_3_0 = new ui.literal(context, "改为指定性别领取");
  node_0_1_3.appendChild(node_0_1_3_0);

  node_0_1.appendChild(node_0_1_3);

  const node_0_1_4 = new ui.picker(context);
  node_0_1_4.data = data;
  node_0_1_4.bindChange = "bindPickerChange";
  node_0_1_4.className = "award-content-l award-content-r";
  node_0_1_4.range = VUE.render(data, "{{array}}");
  node_0_1_4.keys = ["array"];

  const node_0_1_4_0 = new ui.view(context);
  node_0_1_4_0.data = data;
  node_0_1_4_0.className = "award-content-l-loga";

  const node_0_1_4_0_0 = new ui.literal(context, VUE.render(data, "\n        {{array[sex]}}\n      "));
  node_0_1_4_0_0.keys = ["array", "sex"];
  node_0_1_4_0.appendChild(node_0_1_4_0_0);

  node_0_1_4.appendChild(node_0_1_4_0);

  const node_0_1_4_1 = new ui.image(context);
  node_0_1_4_1.data = data;
  node_0_1_4_1.className = "upload";
  node_0_1_4_1.src = "../../images/hs5.png";
  node_0_1_4.appendChild(node_0_1_4_1);

  node_0_1.appendChild(node_0_1_4);

  node_0.appendChild(node_0_1);

  const node_0_2 = new ui.view(context);
  node_0_2.data = data;
  node_0_2.hidden = VUE.render(data, "{{isPacket == 0}}");
  node_0_2.className = "getbull";
  node_0_2.keys = ["isPacket"];

  const node_0_2_0 = new ui.image(context);
  node_0_2_0.data = data;
  node_0_2_0.hidden = VUE.render(data, "{{isPacket == 2}}");
  node_0_2_0.className = "redPacket";
  node_0_2_0.mode = "widthFix";
  node_0_2_0.src = "../../images/hs12.png";
  node_0_2_0.keys = ["isPacket"];
  node_0_2.appendChild(node_0_2_0);

  const node_0_2_1 = new ui.view(context);
  node_0_2_1.data = data;
  node_0_2_1.bindTap = "openPacket";
  node_0_2_1.className = "getbull_but";
  node_0_2.appendChild(node_0_2_1);

  if (VUE.render(data, "{{isPacket == 2}}")) {
    const node_0_2_2 = new ui.image(context);
    node_0_2_2.data = data;
    node_0_2_2.className = "redPacket";
    node_0_2_2.mode = "widthFix";
    node_0_2_2.src = "../../images/hs16.gif";
    node_0_2_2.keys = ["isPacket"];
    node_0_2.appendChild(node_0_2_2);
  }

  const node_0_2_3 = new ui.image(context);
  node_0_2_3.data = data;
  node_0_2_3.bindTap = "closePacket";
  node_0_2_3.className = "close";
  node_0_2_3.mode = "widthFix";
  node_0_2_3.src = "../../images/hs14.png";
  node_0_2.appendChild(node_0_2_3);

  if (VUE.render(data, "{{isPacket == 2 && istext}}")) {
    const node_0_2_4 = new ui.view(context);
    node_0_2_4.data = data;
    node_0_2_4.className = "getbull_info";
    node_0_2_4.keys = ["isPacket", "istext"];

    const node_0_2_4_0 = new ui.literal(context, "已存入零钱，可直接提现");
    node_0_2_4.appendChild(node_0_2_4_0);

    node_0_2.appendChild(node_0_2_4);
  }

  if (VUE.render(data, "{{isPacket == 2 && istext}}")) {
    const node_0_2_5 = new ui.view(context);
    node_0_2_5.data = data;
    node_0_2_5.className = "getbull_money";
    node_0_2_5.keys = ["isPacket", "istext"];

    const node_0_2_5_0 = new ui.text(context);
    node_0_2_5_0.data = data;
    node_0_2_5_0.className = "getbull_money_uu";

    const node_0_2_5_0_0 = new ui.literal(context, VUE.render(data, "{{money}}"));
    node_0_2_5_0_0.keys = ["money"];
    node_0_2_5_0.appendChild(node_0_2_5_0_0);

    node_0_2_5.appendChild(node_0_2_5_0);

    const node_0_2_5_1 = new ui.text(context);
    node_0_2_5_1.data = data;
    node_0_2_5_1.className = "getbull_money_to";

    const node_0_2_5_1_0 = new ui.literal(context, " 元");
    node_0_2_5_1.appendChild(node_0_2_5_1_0);

    node_0_2_5.appendChild(node_0_2_5_1);

    node_0_2.appendChild(node_0_2_5);
  }

  node_0.appendChild(node_0_2);

  const node_0_3 = new ui.button(context);
  node_0_3.data = data;
  node_0_3.hidden = VUE.render(data, "{{!isLogin}}");
  node_0_3.bindTap = "fahong";
  node_0_3.hoverClass = "none";
  node_0_3.keys = ["isLogin"];

  const node_0_3_0 = new ui.literal(context, "发红包");
  node_0_3.appendChild(node_0_3_0);

  node_0.appendChild(node_0_3);

  const node_0_4 = new ui.button(context);
  node_0_4.data = data;
  node_0_4.hidden = VUE.render(data, "{{isLogin}}");
  node_0_4.hoverClass = "none";
  node_0_4.bindGetuserinfo = "onGotUserInfo";
  node_0_4.openType = "getUserInfo";
  node_0_4.keys = ["isLogin"];

  const node_0_4_0 = new ui.literal(context, "红包");
  node_0_4.appendChild(node_0_4_0);

  node_0.appendChild(node_0_4);

  node.appendChild(node_0);
  const node_1 = new ui.text(context);
  node_1.data = data;
  node_1.style = "font-size: 30rpx";
  node_1.hidden = VUE.render(data, "{{istt}}");
  node_1.keys = ["istt"];

  const node_1_0 = new ui.literal(context, "\n  红包规则：\n\n  1.每个用户每天可免费获得五次红包机会，还可以通过观看广告获得额外机会，每人每天最多获得七次红包机会，每日0点更新。\n\n  2.红包后，用户需要观看广告才能获得奖品，退出视为自动放弃。\n\n  3.如发现有恶意刷的行为，简书有权不发放或撤回发放的奖品。\n\n  4.此活动解释权归简书所有。\n\n  5.本次活动与苹果公司无关。\n\n  作者：简书钻首席小管家\n  链接：https://www.jianshu.com/p/3bc50b869c89\n  来源：简书\n  著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。\n");
  node_1.appendChild(node_1_0);

  node.appendChild(node_1);
}

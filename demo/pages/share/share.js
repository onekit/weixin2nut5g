import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
export default OnekitPage({
  data: {
    packet_id: ''
  },
  onLoad(options) {
    this.setData({
      packet_id: options.packet_id
    });
  },
  onReady() {},
  onShow() {},
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {
    return {
      title: '快快来红包',
      path: '/pages/award/award?packet_id=' + this.data.packet_id
    };
  }
});

import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "share";

  const node_0_0 = new ui.navigator(context);
  node_0_0.data = data;
  node_0_0.openType = "navigateBack";
  node_0_0.className = "wait";

  const node_0_0_0 = new ui.literal(context, "稍后分享");
  node_0_0.appendChild(node_0_0_0);

  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.button(context);
  node_0_1.data = data;
  node_0_1.openType = "share";
  node_0_1.className = "now";

  const node_0_1_0 = new ui.literal(context, "马上分享");
  node_0_1.appendChild(node_0_1_0);

  node_0.appendChild(node_0_1);

  node.appendChild(node_0);
}

import ui from '../../../ui/index.js'
import VUE from '../../../VUE.js'
export default function (context, node, data) {
  const node_0 = new ui.view(context);
  node_0.data = data;
  node_0.className = "main";

  const node_0_0 = new ui.view(context);
  node_0_0.data = data;
  node_0_0.className = "title";

  const node_0_0_0 = new ui.view(context);
  node_0_0_0.data = data;
  node_0_0_0.className = "title_left";

  const node_0_0_0_0 = new ui.literal(context, "我的钱包");
  node_0_0_0.appendChild(node_0_0_0_0);

  node_0_0.appendChild(node_0_0_0);

  node_0.appendChild(node_0_0);

  const node_0_1 = new ui.view(context);
  node_0_1.data = data;
  node_0_1.className = "tixian";

  const node_0_1_0 = new ui.view(context);
  node_0_1_0.data = data;
  node_0_1_0.className = "tixian_left";

  const node_0_1_0_0 = new ui.image(context);
  node_0_1_0_0.data = data;
  node_0_1_0_0.src = "../../images/hs2.png";
  node_0_1_0.appendChild(node_0_1_0_0);

  const node_0_1_0_1 = new ui.literal(context, VUE.render(data, "\n            {{balance}}\n        "));
  node_0_1_0_1.data = data;
  node_0_1_0_1.keys = ["balance"];
  node_0_1_0.appendChild(node_0_1_0_1);

  node_0_1.appendChild(node_0_1_0);

  node_0.appendChild(node_0_1);

  const node_0_2 = new ui.button(context);
  node_0_2.data = data;
  node_0_2.bindTap = "insetMoney";
  node_0_2.className = "insetMoney";

  const node_0_2_0 = new ui.literal(context, "充值");
  node_0_2.appendChild(node_0_2_0);

  node_0.appendChild(node_0_2);

  const node_0_3 = new ui.button(context);
  node_0_3.data = data;
  node_0_3.bindTap = "tixian";
  node_0_3.className = "embody";

  const node_0_3_0 = new ui.literal(context, "提现");
  node_0_3.appendChild(node_0_3_0);

  node_0.appendChild(node_0_3);

  node.appendChild(node_0);
}

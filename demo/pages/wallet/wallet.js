import OnekitPage from '../../../OnekitPage.js';
import wx from '../../../wx.js';
import macro from '../../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
import {
  getUserInfo,
  extract
} from "../../utils/request.js";
const app = getApp();
export default OnekitPage({
  data: {
    balance: '0.00'
  },
  onLoad(options) {},
  onReady() {},
  insetMoney() {
    wx.navigateTo({
      url: '/pages/pushMoney/pushMoney'
    });
  },
  onShow() {
    getUserInfo({}, (data) => {
      if (data.code == 0) {
        this.setData({
          balance: data.data.balance
        });
        wx.setStorageSync('balance', data.data.balance);
        wx.setStorageSync('openid', data.data.openid);
      }
    });
  },
  tixian() {
    wx.navigateTo({
      url: '/pages/tixian/tixian?money=' + this.data.balance
    });
  },
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {}
});

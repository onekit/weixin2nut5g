import wx from '../../wx.js';
import macro from '../../macro.js';
const {
  Any,
  getApp,
  getCurrentPages
} = macro;
let global = {};
const app = getApp();
const host = 'https://crm.aiheisha.net';

function request(url, data, success) {
  var token = wx.getStorageSync('token');
  if (url == '/register/resetPass.html') {
    token = wx.getStorageSync('token1');
  };
  wx.request({
    url: host + url,
    header: {
      "Content-Type": "application/x-www-form-urlencoded",
      'token': token
    },
    data,
    method: 'POST',
    success(res) {
      if (res.data.code == 101) {
        wx.showModal({
          title: '提示',
          content: '登录后继续操作',
          confirmText: '去登录',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定');
              wx.navigateTo({
                url: '/pages/login/login' + url == '/applets/redpacket/RedPackets/getPacketInfo' ? '?packet_id=' + data.packet_id : ''
              });
            } else if (res.cancel) {
              console.log('用户点击取消');
            };
          }
        });
      } else {
        success(res.data);
      };
    },
    fail() {
      console.log('失败');
    }
  });
};
export const wxLogin = (data, success) => {
  request('/applets/redpacket/Author/getWxSession', data, success)
};
export const code2Session = (data, success) => {
  request('/applets/redpacket/Author/code2Session', data, success)
};
export const createRedPacket = (data, success) => {
  request('/applets/redpacket/RedPackets/createRedPacket', data, success)
};
export const BillLogs = (data, success) => {
  request('/applets/redpacket/Bill/logs', data, success)
};
export const extract = (data, success) => {
  request('/applets/redpacket/Pay/extract', data, success)
};
export const createOrder = (data, success) => {
  request('/applets/redpacket/Pay/createOrder', data, success)
};
export const getUserInfo = (data, success) => {
  request('/applets/redpacket/User/getUserInfo', data, success)
};
export const getPacket = (data, success) => {
  request('/applets/redpacket/RedPackets/getPacket', data, success)
};
export const getPacketInfo = (data, success) => {
  request('/applets/redpacket/RedPackets/getPacketInfo', data, success)
};
export const redPacketList = (data, success) => {
  request('/applets/redpacket/RedPackets/redPacketList', data, success)
};

import OnekitApp from '../OnekitApp.js';
import wx from '../wx.js';
import macro from '../macro.js';const {Any,getApp,getCurrentPages} = macro;
let global = {};
export default OnekitApp({
    onLaunch(){
      wx.getSystemInfo({
        success(res){
          wx.setStorageSync('isqiye',res.environment == 'wxwork'?1:2);
        }
      });
    },
    globalData:{
        height_01:"",
        userInfo:null,
        toast:(content,icon,duration,mask)=>{wx.showToast({
            title:content,
            icon:icon?icon:"none",
            duration:duration?duration:2000,
            mask:mask?mask:false
          })},
        loading:(content,mask)=>{wx.showLoading({
            title:content?content:'拼命加载中...',
            mask:mask?mask:true
          })}
      }
  });

export default {
  "tabBar": {
    "color": "#bec3ce",
    "selectedColor": "#e55656",
    "borderStyle": "black",
    "list": [{
      "text": "发红包",
      "pagePath": "pages/award/award",
      "iconPath": "images/hs7.png",
      "selectedIconPath": "images/hs6.png"
    }, {
      "text": "我的",
      "pagePath": "pages/main/main",
      "iconPath": "images/hs8.png",
      "selectedIconPath": "images/hs9.png"
    }]
  },
  "pages": ["pages/award/award", "pages/login/login", "pages/main/main", "pages/wallet/wallet", "pages/definite/definite", "pages/pushMoney/pushMoney", "pages/tixian/tixian", "pages/share/share", "pages/info/info", "pages/detail/detail"],
  "window": {
    "component": false,
    "usingComponents": {},
    "navigationBarTextStyle": true,
    "navigationBarTitleText": "发红包",
    "navigationStyle": false,
    "backgroundColor": {
      "value": -1,
      "falpha": 0.0
    },
    "backgroundTextStyle": false,
    "backgroundColorTop": {
      "value": -1,
      "falpha": 0.0
    },
    "backgroundColorBottom": {
      "value": -1,
      "falpha": 0.0
    },
    "enablePullDownRefresh": false,
    "onReachBottomDistance": 50.0,
    "pageOrientation": "portrait",
    "disableScroll": false,
    "allowsBounceVertical": false,
    "transparentTitle": "",
    "titlePenetrate": false,
    "showTitleLoading": false,
    "gestureBack": false,
    "enableScrollBar": false
  }
}

import fs from "fs";
import path from "path";
import APP_JSON from "../app.json.js"
import account from "./Nut5GAccount.js"
import TheKit from "nodejs-thekit"
import nut5g_cloud from "nut5g-cloud";
import nut5g_cloud_sdk from "nut5g-cloud-sdk"
const {
  STRING,
  FileDB,
  TASK
} = TheKit
const {
  ChatbotInfoMenuRequest,
  AccessTokenRequest,
} = nut5g_cloud.com.msg5g.maap.request
const {
  Nut5GSDK
} = nut5g_cloud_sdk.com.msg5g.maap
const {
  Suggestion,
  Reply,
  Content,
  Layout
} = nut5g_cloud.com.msg5g.maap.request.entry
const medias = {}
const __dirname = path.resolve(path.dirname(''));
const sdk = new Nut5GSDK(account)
const accessToken = await _checkAccessToken();
////////////////////////////////////////
async function _checkAccessToken() {
  return new Promise((callback) => {
    const request = new AccessTokenRequest();
    request.appId = account.appid;
    request.appKey = account.appKey;

    sdk.accessToken(request, (response) => {
      const accessToken = response.accessToken
      const accessToken_expire = new Date().getTime() + response.expires * 1000
      console.log("accessToken,accessToken_expire", accessToken, accessToken_expire)
      process.accessToken = accessToken
      process.accessToken_expire = accessToken_expire
      //console.log("tttttttttttttttt1",process.accessToken,response.expires)
      callback(accessToken);
    });
  })
}
async function fastsdk_upload(url, file) {
  return new Promise((callback) => {
    sdk.mediasUpload(accessToken, "temp", {
      "file1": file
    }, (response) => {
      const fileInfo = response.fileInfo[0]
      console.log("fileInfo=============", fileInfo)
      //
      const medias_path = `${__dirname}/onekit/data/medias.json`
      const medias = JSON.parse(fs.readFileSync(medias_path))
      medias[url] = fileInfo
      fs.writeFileSync(medias_path, JSON.stringify(medias))
      //
      callback(fileInfo)
    });
  })
}
async function fastsdk_menu(items) {
  return new Promise((callback) => {
    const request = new ChatbotInfoMenuRequest();
    const entries = new ChatbotInfoMenuRequest.BigMenu.Entry();
    const bigMenu = new ChatbotInfoMenuRequest.BigMenu();
    const smallMenu = new ChatbotInfoMenuRequest.BigMenu.Entry.SmallMenu();

    const menu = []
    for (const item of items) {
      const entry = new ChatbotInfoMenuRequest.BigMenu.Entry();
      const json1 = {
        type: "navigateTo"
      };
      const info1 = {
        url: item.url
      };
      json1.info = info1
      const reply1 = new Reply(STRING.stringToBase64(JSON.stringify(json1)), item.title);
      entry.reply = reply1;
      menu.push(entry)
    }
    smallMenu.displayText = "主菜单";
    smallMenu.entries = menu;
    entries.menu = smallMenu;

    bigMenu.entries = [entries];

    request.menu = bigMenu;
    console.log(request);
    sdk.chatbotInfoMenu(accessToken, request, callback);
  });
}
async function mapDir(dir) {
  const files = fs.readdirSync(dir)
  for (const filename of files) {
    let pathname = path.join(dir, filename)
    const stats = fs.statSync(pathname)
    if (stats.isDirectory()) {
      if (['node_modules', 'cloudfunctions', 'onekit'].includes(pathname)) {
        continue
      }
      console.log("[DIR]", pathname)
      await mapDir(pathname)
    } else if (stats.isFile()) {
      if (['.DS_Store', ".db"].includes(filename)) {
        continue
      }
      switch (path.extname(pathname)) {
        case ".jpg":
        case ".jpeg":
        case ".gif":
        case ".png":
          const fullpath = `${__dirname}/${pathname}`
          console.log("[FILE]", pathname, fullpath)
          const info = await fastsdk_upload(pathname, fullpath, "perm")
          medias[pathname] = {
            value: info,
            title: new Date().getTime()
          }
          break;
        default:
          break;
      }
    }
  }
}
export default async function (Nut5GAccount) {
  const data_folder = `${__dirname}/onekit/data`
  ////////////////////////
  const menus = APP_JSON.tabBar ? APP_JSON.tabBar.list.map((item) => {
    return {
      "title": item.text,
      "url": item.pagePath
    }
  }) : [{
    "title": "启动",
    "url": APP_JSON.pages[0]
  }]
  fastsdk_menu(menus)

  ////////////////////////
  const medias_path = `${data_folder}/medias.json`
  if (!fs.existsSync(medias_path)) {
    fs.writeFileSync(medias_path, "{}")
    await mapDir("./")
  }
}

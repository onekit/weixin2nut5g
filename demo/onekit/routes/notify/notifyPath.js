import Nut5GAccount from "../../Nut5GAccount.js"
import not5g_cloud_sdk from "nut5g-cloud-sdk";
const NotifyPathSDK = not5g_cloud_sdk.com.msg5g.maap.notification.NotifyPathSDK
import TheKit from "nodejs-thekit";
const {
  FileDB,
  AJAX
} = TheKit
import {
  Router
} from 'express';
var router = Router();

router.get('/', function (request, response) {

  const data = new NotifyPathSDK(Nut5GAccount, request, response).notifyPath();
  //console.log(data);
  new FileDB(Nut5GAccount.appid).set("notify_notifyPath", new Date().toString(), JSON.stringify(data));
  response.send(data)
});

export default router;

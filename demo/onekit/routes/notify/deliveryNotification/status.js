import TheKit from "nodejs-thekit";
const {
  FileDB
} = TheKit;
import Nut5GAccount from "../../../Nut5GAccount.js"
import not5g_cloud_sdk from "nut5g-cloud-sdk";
const DeliveryNotificationSDK = not5g_cloud_sdk.com.msg5g.maap.notification.DeliveryNotificationSDK

import {
  Router
} from 'express';

var router = Router();

router.post('/', function (request, response) {
  const data = new DeliveryNotificationSDK(Nut5GAccount, request, response).status();
  //console.log(data);
  new FileDB(Nut5GAccount.appid).set("notify_deliveryNotificationsdk_status", new Date().toString(), JSON.stringify(data));
  response.send(data)
});

export default router;

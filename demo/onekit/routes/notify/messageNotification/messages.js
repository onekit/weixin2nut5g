import TheKit from "nodejs-thekit";
import fs from "fs"
import path from "path"
import wx from "../../../../../wx.js"
const __dirname = path.resolve(path.dirname(''));
const {
  AJAX
} = TheKit;
import Nut5GAccount from "../../../Nut5GAccount.js"
import not5g_cloud_sdk from "nut5g-cloud-sdk";
const MessageNotificationSDK = not5g_cloud_sdk.com.msg5g.maap.notification.MessageNotificationSDK

import {
  Router
} from 'express';
import OneKit from "../../../../../x2nut5g/OneKit.js";

import FileDBNut5GFastSDK from "../../../FileDBNut5GFastSDK.js";
var router = Router();

await router.post('/', async function (request, response) {
  const data = new MessageNotificationSDK(Nut5GAccount, request).receivemessage();
  response.send("")
  global.address = data.senderAddress
  ////////////
  const userDir = path.join(__dirname, "onekit", "data", global.address)
  if (!fs.existsSync(userDir)) {
    fs.mkdirSync(userDir)
  }
  /////////////////////////
  const session = OneKit.session()
  if (!session.fastsdk) {
    session.fastsdk = new FileDBNut5GFastSDK(Nut5GAccount, global.address)
  }
  ///////////////////////////

  ///////////////////////////
  const postback = session.fastsdk.postback(data)
  if (typeof (postback) === "string") {
    if (OneKit.session().currentPage.formRowCallback) {
      OneKit.session().currentPage.formRowCallback(postback)
    }
    return
  }
  switch (postback.type) {
    case "switchTab":
    case "reLaunch":
    case "redirectTo":
    case "navigateTo":
    case "navigateBack":
      postback.info.success = console.log
      postback.info.fail = console.error
      await wx[postback.type](postback.info)
      break
    case "event":
      const page = OneKit.session().pages[postback.page_id]
      const ui = page.uis[postback.ui_id]
      page[postback.handler]({
        currentTarget: ui
      })
      ui.invokeInnterHandler(postback.event)
      break
    case "row":
      break;
    default:
      throw new Error(postback.type);
  }
});

export default router;

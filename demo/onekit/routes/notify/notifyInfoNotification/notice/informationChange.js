import TheKit from "nodejs-thekit";
const {
  FileDB,
  AJAX
} = TheKit;
import Nut5GAccount from "../../../../Nut5GAccount.js"
import not5g_cloud_sdk from "nut5g-cloud-sdk";
const NotifyInfoNotificationSDK = not5g_cloud_sdk.com.msg5g.maap.notification.NotifyInfoNotificationSDK

import {
  Router
} from 'express';
var router = Router();

router.post('/', function (request, response) {

  const data = new NotifyInfoNotificationSDK(Nut5GAccount, request, response).informationChange();
  new FileDB(Nut5GAccount.appid).set("notify_notifyInfoNotification_notice_informationchanage", new Date().toString(), JSON.stringify(data));
  response.send(data)
});

export default router;

#!/usr/bin/env node

/* eslint-disable  */

/**
 * Module dependencies.
 */

import app from '../app.js';
import fs from 'fs';
import DEBUG from 'debug';
const debug = DEBUG('demo:httpServer');
import http from 'http';
import https from 'https';
import path from 'path';
const __dirname = path.resolve(path.dirname(''));
/**
 * Get SSLPORT from environment and store in Express.
 */
const httpsOption = {
  key: fs.readFileSync(path.join(__dirname, 'onekit/https/app.onekit.cn.key')),
  cert: fs.readFileSync(path.join(__dirname, 'onekit/https/app.onekit.cn.pem'))
}
//var SSLPORT = normalizePort( '9443');
//app.set('SSLPORT', SSLPORT);

/**
 * Create HTTP httpServer.
 */

//var httpServer = http.createServer(app);

/**
 * Listen on provided SSLPORT, on all network interfaces.
 */

//httpServer.listen(SSLPORT);

var httpServer = http.createServer(app);
var httpsServer = https.createServer(httpsOption, app);
var PORT = 8080;
var SSLPORT = 9443;

httpServer.listen(PORT, function () {
  //console.log('HTTP Server is running on: http://localhost:%s', PORT);
});
httpsServer.listen(SSLPORT, function () {
  //console.log('HTTPS Server is running on: https://localhost:%s', SSLPORT);
});
httpServer.on('error', onError);
httpServer.on('listening', onListening);

/**
 * Normalize a SSLPORT into a number, string, or false.
 */

function normalizePort(val) {
  var SSLPORT = parseInt(val, 10);

  if (isNaN(SSLPORT)) {
    // named pipe
    return val;
  }

  if (SSLPORT >= 0) {
    // SSLPORT number
    return SSLPORT;
  }

  return false;
}

/**
 * Event listener for HTTP httpServer "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof SSLPORT === 'string' ?
    'Pipe ' + SSLPORT :
    'Port ' + SSLPORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP httpServer "listening" event.
 */

function onListening() {
  var addr = httpServer.address();
  var bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'SSLPORT ' + addr.SSLPORT;
  debug('Listening on ' + bind);
}

import Nut5GAccount from "./Nut5GAccount.js"
import go from "./go.js"
await go(Nut5GAccount)
////////////////////////////
import createError from 'http-errors';
import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import path from 'path';
const __dirname = path.resolve(path.dirname(''));
import indexRouter from './routes/index.js';
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'onekit/views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
/////////////////////////////////
import APP_JSON from "../app.json.js"
process.APP_JSON = APP_JSON
//
import "../app.js"
//
import deliveryNotification_status from './routes/notify/deliveryNotification/status.js';
import messageNotification_messages from './routes/notify/messageNotification/messages.js';
import notifyInfoNotification_check from './routes/notify/notifyInfoNotification/check.js';
import notifyInfoNotification_notice_informationChange from './routes/notify/notifyInfoNotification/notice/informationChange.js';
import notifyInfoNotification_notice_rcsspam from './routes/notify/notifyInfoNotification/notice/rcsspam.js';
import notifyPath from './routes/notify/notifyPath.js';
app.use(`/nut5g/deliveryNotification/${Nut5GAccount.chatbotId}/status`, deliveryNotification_status);
app.use(`/nut5g/messageNotification/${Nut5GAccount.chatbotId}/messages`, messageNotification_messages);
app.use(`/nut5g/notifyInfoNotification/${Nut5GAccount.chatbotId}/check`, notifyInfoNotification_check);
app.use(`/nut5g/notifyInfoNotification/${Nut5GAccount.chatbotId}/notice/informationChange`, notifyInfoNotification_notice_informationChange);
app.use(`/nut5g/notifyInfoNotification/${Nut5GAccount.chatbotId}/notice/rcsspam`, notifyInfoNotification_notice_rcsspam);
app.use('/nut5g/notifyPath', notifyPath);
//////////
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  console.error(req.url)
  next(createError(404));
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



export default app;

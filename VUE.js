import TheKit from "nodejs-thekit"
import path from "path"
import mustache2 from "./js/mustache2.js"
const {
  OBJECT,
  PATH
} = TheKit
const __dirname = path.resolve(path.dirname(''));
import FOR from "./ui/for.js"
export default class VUE {

  static render(data, template) {
    if (!data) {
      return null
    }
    //template = template.replace(new RegExp("]", "gm"), "+1]")
    let result = ""
    let i = 0
    let p
    while ((p = template.indexOf("{{", i)) >= 0) {
      if (p > i) {
        result += template.substring(i, p)
      }
      let q = template.indexOf("}}", p)
      if (q > p) {
        if (p == 0 && q == (template.length - 2)) {
          result = mustache2.render(template, data);
          //  console.log("ttttttttt", template, typeof(result),result)

          return result
        }
        const value = template.substring(p + 2, q)
        result += mustache2.render(`{{${value}}}`, data);
        i = q + 2
      } else {
        break
      }
    }
    result += template.substring(i)
    //console.log("rrrrrrrrrrrr", result, template)
    return result
  }
  /*
    static logic(data) {
      return data != null
    }*/
  static include(url, node, data) {
    //console.log("---------",node.context.route,url,PATH.rel2abs(`/${node.context.route}.js`, url))
    ;
    (async function () {
      const src = path.join(__dirname, '/') + PATH.rel2abs(`/${node.context.route}.js`, url)
      return import(src)
    })(node, data);
  }
  static For(THIS, data, itemsKey, key, index, item, parent, callback) {
    if (!data) {
      return data
    }
    let items = data
    for (const path of itemsKey.split(".")) {
      items = items[path]
    }
    if (!items) {
      return
    }
    const onekit_for = new FOR(parent.context)
    parent.appendChild(onekit_for)
    for (let i = 0; i < items.length; i++) {
      data[index] = i;
      data[item] = items[i]
      callback.call(THIS, data, onekit_for)
    }
  }
}

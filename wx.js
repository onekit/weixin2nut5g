import THEKIT from 'nodejs-thekit'
const {
  PROMISE,
  STRING,
  TASK
} = THEKIT
import TheKit from './js/TheKit.js'
import OneKit from './OneKit.js'
import NodejsPage from "./NodejsPage.js"
import EventChannel from "./api/EventChannel.js"
import CanvasContext from './api/CanvasContext.js'
import Context from './api/Context.js'
import OffscreenCanvas from './api/OffscreenCanvas.js'
import CameraContext from './api/CameraContext.js'
import EditorContext from './api/EditorContext.js'
import MapContext from "./api/MapContext.js"
import SelectorQuery from "./api/SelectorQuery.js"
import Animation from './api/Animation.js'
import LogManager from './api/LogManager.js'
import RequestTask from './api/RequestTask.js'
import SocketTask from './api/SocketTask.js'
import UpdateManager from './api/UpdateManager.js'
import DownloadTask from './api/DownloadTask.js'
import UploadTask from './api/UploadTask.js'
import RecorderManager from "./api/RecorderManager.js"
import BackgroundManager from './api/BackgroundAudioManager.js'
import InnerAudioContext from './api/InnerAudioContext.js'
import VideoContext from './api/VideoContext.js'
import LivePusherContext from './api/LivePusherContext.js'
import LivePlayerContext from './api/LivePusherContext.js'
import FileSystemManager from './api/FileSystemManager.js'
import MediaContainer from './api/MediaContainer.js'
import _MediaRecorder from './api/MediaRecorder.js'
import VideoDecoder from './api/VideoDecoder.js'
import BLEPeripheralServer from './api/BLEPeripheralServer.js'
import AudioContext from './api/AudioContext.js'
import MediaRecorder from './api/MediaRecorder.js'
import wx_cloud from './serverless/wx.cloud.js'
import axios from "axios"
import localStorage from "./js/localStorage.js"
class WX {
  constructor() {}
  static get cloud() {
    return wx_cloud
  }
  /** 基础 */
  canIUse() {
    return true
  }

  base64ToArrayBuffer(base64) {
    return STRING.base64ToArrayBuffer(base64)
  }

  arrayBufferToBase64(arrayBuffer) {
    return STRING.arrayBufferToBase64(arrayBuffer)
  }
  /** 更新 */
  updateWeChatApp(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getUpdateManager() {
    return new UpdateManager()
  }

  UpdateManager(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }
  /** 生命周期 */
  getLaunchOptionsSync() {
    return this.fn_global().OPTION
  }

  getEnterOptionsSync() {
    return this.fn_global().OPTION
  }

  exitMiniProgram() {

  }

  canIPutStuffOverComponent() {
    return true
  }
  /** 应用级事件 */

  onUnhandledRejection(wx_callback) {
    this.fn_global().onUnhandledRejection = wx_callback
  }

  onThemeChange(wx_callback) {
    // this.fn_global().onThemeChange = wx_callback
  }

  onPageNotFound(wx_callback) {
    this.fn_global().onPageNotFound = wx_callback
  }

  onError(wx_callback) {
    this.fn_global().onError = wx_callback
  }

  onAudioInterruptionEnd(wx_callback) {
    this.fn_global().onAudioInterruptionEnd = wx_callback
  }

  onAudioInterruptionBegin(wx_callback) {
    this.fn_global().onAudioInterruptionBegin = wx_callback
  }

  onAppShow(wx_callback) {
    this.fn_global().onAppShow = wx_callback
  }

  onAppHide(wx_callback) {
    this.fn_global().onAppHide = wx_callback
  }

  offUnhandledRejection() {
    this.fn_global().onUnhandledRejection = NaN
  }

  offThemeChange() {
    this.fn_global().onThemeChange = null
  }

  offPageNotFound() {
    this.fn_global().onPageNotFound = null
  }

  offError() {
    this.fn_global().onError = null
  }

  offAudioInterruptionEnd() {
    this.fn_global().onAudioInterruptionBegin = null
  }

  offAudioInterruptionBegin() {
    this.fn_global().offAudioInterruptionBegin = null
  }

  offAppShow() {
    this.fn_global().onAppShow = null
  }

  offAppHide() {
    this.fn_global().onAppHide = null
  }
  /** 环境变量 */

  get env() {
    const VERSION = 'production'
    const USER_DATA_PATH = 'wxfile://user'
    const obj = {
      VERSION,
      USER_DATA_PATH,
    }
    return Object(obj)
  }

  /** 调试 */
  setEnableDebug(wx_object) {
    const wx_enableDebug = wx_object.enableDebug
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getRealtimeLogManager() {
    return new LogManager()
  }

  getLogManager(wx_object) {
    const wx_level = wx_object.level ? wx_object.level : 0
    const reg = /[0-1]/
    if (wx_level.natch(reg)) {
      return new LogManager()
    } else {
      return false
    }
  }

  LogManager() {
    return new LogManager()
  }

  RealtimeLogManager() {
    return new LogManager()
  }


  enableAlertBeforeUnload(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  disableAlertBeforeUnload(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail; 
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  createAnimation(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /** 网络 */
  request(wx_object) {
    const url = wx_object.url
    const data = wx_object.data
    const header = wx_object.header
    const method = wx_object.method || 'GET'
    const timeout = wx_object.timeout
    const wx_responseType = wx_object.responseType || 'text'
    const wx_dataType = wx_object.dataType || 'json'
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    //
    //  let wx_enableHttp2 = wx_object.enableHttp2
    // let wx_enableQuic = wx_object.enableQuic
    //   let wx_enableCahe = wx_object.enableChache
    wx_object = null
    // ////////////////////////
    let vue_responseType
    switch (wx_responseType) {
      case 'text':
        switch (wx_dataType) {
          case 'json':
            vue_responseType = 'json'
            break
          default:
            vue_responseType = 'text'
            break
        }
        break
      case 'arraybuffer':
        vue_responseType = 'arraybuffer'
        break
      default:
        throw wx_responseType
    }

    const axios_instance = axios.create({
      data: data,
      headers: header,
      timeout: timeout,
      method: method,
      responseType: vue_responseType,
    })

    const requestTask = new RequestTask(axios_instance)

    setTimeout(() => {
      axios_instance({
        url: url,

      }).then((response) => {
        const wx_res = {
          cookies: response.cookies || [],
          data: response.data || null,
          errMsg: `request: ${response.statusText}`,
          header: response.headers,
          statusCode: response.status,
        }
        if (wx_success) {
          wx_success(wx_res)
        }
        if (wx_complete) {
          wx_complete(wx_res)
        }
      }).catch((error) => {
        if (wx_fail) {
          wx_fail(error)
        }
        if (wx_complete) {
          wx_complete(error)
        }
      })
    }, 500)

    return requestTask
  }


  downloadFile(wx_object) {
    const wx_url = wx_object.url
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    //
    const wx_header = wx_object.header
    const wx_mehotd = wx_object.method || 'GET'

    const axios_instance = axios.create({
      headers: wx_header,
      responseType: 'blob',
    })

    const downloadTask = new DownloadTask(axios_instance)
    setTimeout(() => {
      axios_instance({
        url: wx_url,
        method: wx_mehotd,
      }).then((res) => {
        const tempFilePath = TheKit.createTempPath(wx_url.substr(wx_url.lastIndexOf('/')))
        this.fn_global().TEMP[tempFilePath] = res.data
        if (wx_success) {
          wx_success({
            tempFilePath,
          })
        }
        if (wx_complete) {
          wx_complete()
        }
      }).catch((err) => {
        if (wx_fail) {
          wx_fail({
            errMsg: err.message,
          })
        }
        if (wx_complete) {
          wx_complete()
        }
      })
    }, 500)


    return downloadTask
  }


  uploadFile(wx_object) {
    const url = wx_object.url
    const filePath = wx_object.filePath
    const name = wx_object.name
    let header = wx_object.header
    const formData = wx_object.formData
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null
    // /////////////////

    let blob
    if (filePath.startsWith('wxfile://store/onekit_')) {
      blob = null // sessionStorage.getItem(filePath)
    } else if (filePath.startsWith('wxfile://tmp_onekit_')) {
      blob = this.fn_global().TEMP[filePath]
    } else {
      throw new Error(filePath)
    }
    if (!header) {
      header = {}
    }
    header['Content-Type'] = 'multipart/form-data'
    // ///////////////
    const data = new FormData()

    const fData = new File([blob], filePath)

    data.append(name, fData, filePath)


    // if(formData) {
    // for (const key of Object.keys(formData)) {
    //   data.append(key, formData[key])
    // }
    // console.log(data)
    // }

    const axios_instance = axios.create({
      headers: header,
    })

    const uploadTask = new UploadTask(axios_instance)

    setTimeout(() => {
      axios_instance({
        url,
        data,
        method: 'post',
        // ...config
      }).then((res) => {
        if (wx_success) {
          wx_success(res)
        }
        if (wx_complete) {
          wx_complete(res)
        }
      }).catch((err) => {
        if (wx_fail) {
          wx_fail(err)
        }
        if (wx_complete) {
          wx_complete(err)
        }
      })
    }, 0)

    return uploadTask
  }

  connectSocket(wx_object) {
    const wx_url = wx_object.url
    const wx_protocols = wx_object.protocols
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onSocketOpen(callback) {
    if (!this.fn_global()._socket) {
      return false
    }

    this.fn_global()._socket.addEventListener('open', (event) => {
      if (callback) {
        return callback(event)
      }
    })
  }

  sendSocketMessage(wx_object) {
    const wx_data = wx_object.data
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onSocketMessage(callback) {
    if (!this.fn_global()._socket) {
      return null
    }

    this.fn_global()._socket.addEventListener('message', (event) => {
      if (callback) {
        return callback(event)
      }
    })
  }

  onSocketError(callback) {
    if (!this.fn_global()._socket) {
      return null
    }
    this.fn_global()._socket.addEventListener('error', () => {
      if (callback) {
        callback()
      }
    })
  }

  onSocketClose(callback) {
    if (!this.fn_global()._socket.close) {
      return null
    }

    this.fn_global()._socket.addEventListener('close', () => {
      const wx_code = this.fn_global()._socket_closeCode
      const wx_reson = this.fn_global()._socket_coloseReson

      this.fn_global()._socket_closeCode = null
      this.fn_global()._socket_coloseReson = null
      if (callback) {
        callback(wx_code, wx_reson)
      }
    })
  }

  closeSocket() {
    this.fn_global()._socketTask.close()
  }

  offLocalServiceResolveFail() {
    console.error('HTML5 is not support mDNS!!')
  }
  onLocalServiceResolveFail() {
    console.error('HTML5 is not support mDNS!!')
  }
  offLocalServiceDiscoveryStop() {
    console.error('HTML5 is not support mDNS!!')
  }
  onLocalServiceDiscoveryStop() {
    console.error('HTML5 is not support mDNS!!')
  }
  offLocalServiceLost() {
    console.error('HTML5 is not support mDNS!!')
  }
  onLocalServiceLost() {
    console.error('HTML5 is not support mDNS!!')
  }
  offLocalServiceFound() {
    console.error('HTML5 is not support mDNS!!')
  }
  onLocalServiceFound() {
    console.error('HTML5 is not support mDNS!!')
  }
  stopLocalServiceDiscovery() {
    console.error('HTML5 is not support mDNS!!')
  }
  startLocalServiceDiscovery() {
    console.error('HTML5 is not support mDNS!!')
  }

  createUDPSocket() {
    console.error('HTML5 is not support UDP!!')
  }

  setStorageSync(key, value) {
    try {
      localStorage.setItem(key, value)
    } catch (vue_e) {
      const wx_e = vue_e
      throw wx_e
    }
  }

  setStorage(wx_object) {
    if (!wx_object) {
      return
    }
    const wx_key = wx_object.key
    const wx_data = wx_object.data
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getStorageSync(key) {
    return localStorage.getItem(key)
  }

  getStorage(wx_object) {
    const key = wx_object.key
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {
        value: this.getStorageSync(key)
      }
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  removeStorageSync(key) {
    localStorage.removeItem(key)
  }

  removeStorage(wx_object) {
    const key = wx_object.key
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  clearStorageSync() {
    localStorage.clear()
  }

  clearStorage(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getStorageInfo(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getStorageInfoSync() {
    let wx_res
    try {
      const keysArray = new Array()
      for (let i = 0; i < localStorage.length; i++) {
        const getKey = localStorage.key(i)
        keysArray.push(getKey)
      }
      let sizeStore = 0
      if (localStorage) {
        for (const item of Object.keys(localStorage)) {
          sizeStore += localStorage.getItem(item).length
        }
      }
      wx_res = {
        keys: keysArray,
        currentSize: Math.ceil((sizeStore / 1024).toFixed(2)),
        limitSize: '5110',
      }
      return wx_res
    } catch (e) {
      throw new Error(e.message)
    }
  }

  setBackgroundFetchToken(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }
  onBackgroundFetchData() {

  }
  getBackgroundFetchToken(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }
  getBackgroundFetchData(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  saveImageToPhotosAlbum(wx_object) {
    const wx_filePath = wx_object.filePath
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getImageInfo(wx_object) {
    const wx_src = wx_object.src

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  compressImage(wx_object) {
    const wx_src = wx_object.src
    const wx_quality = wx_object.wx_quality || 0.6
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  chooseMessageFile(wx_object) {
    // console.error("[x2x] chooseMessageFile is not surport!!")
    const wx_conunt = wx_object.count
    const wx_type = wx_object.type || 'all'
    const wx_extensions = wx_object.extensions
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }




  chooseImage(wx_object) {
    const wx_count = wx_object.count || 9
    const wx_sizeType = wx_object.sizeType || ['original', 'compressed']
    const wx_sourceType = wx_object.sourceType || ['album', 'camera']
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  previewImage(wx_object) {
    const wx_urls = wx_object.urls
    const wx_current = wx_object.current
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)


  }

  getVideoInfo(wx_object) {
    const wx_src = wx_object.src
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)


  }

  compressVideo() {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  chooseMedia(wx_object) {
    // const wx_count = wx_object.count || 9                                   //
    const wx_mediaType = wx_object.mediaType || ['image', 'video'] //
    const wx_sourceType = wx_object.sourceType || ['album', 'camera'] //
    // const wx_maxDuration = wx_object.maxDuration || 10                      // HTML5 is not support
    // const wx_sizeType = wx_object.signType || ['original', 'compressed']  
    const wx_camera = wx_object.camera || 'back' //
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }




  //////////////////// 录音 //////////////////////////

  startRecord() {

  }

  stopRecord() {

  }

  getRecorderManager() {

  }

  //////////////////// 音频 /////////////////////////

  getBackgroundAudioManager() {

  }

  createInnerAudioContext() {


  }

  createAudioContext(id) {

  }

  createMediaAudioPlayer() {

  }

  //////////////////// 实时音视频 ////////////////////

  createLivePusherContext() {
    return new LivePusherContext()
  }

  createLivePlayerContext(id, component) {
    return new LivePlayerContext(id, component)
  }
  //////////////////// 视频  ///////////////////////

  chooseVideo(wx_object) {
    const sourceType = wx_object.sourceType || ['album', 'camara']
    // const wx_compressed = wx_Object.compressed || true  // HTML5 is not support
    // const wx_maxDuration = wx_object.maxDuration || 60  // HTML5 is not support
    const camera = wx_object.camera || 'back'
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  saveVideoToPhotosAlbum(wx_object) {

    const filePath = wx_object.filePath
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  createVideoContext(id, component) {

  }

  openVideoEditor() {

  }


  ////////////////// 音视频合成  ///////////////////
  createMediaContainer() {
    return new MediaContainer()
  }

  ///////////////// 实时语音 //////////////////////

  updateVoIPChatMuteConfig() {

  }

  subscribeVoIPVideoMembers() {

  }

  onVoIPVideoMembersChanged() {}

  onVoIPChatSpeakersChanged() {}

  onVoIPChatInterrupted() {}

  offVoIPVideoMembersChanged() {}

  offVoIPChatMembersChanged() {}

  offVoIPChatInterrupted() {}

  joinVoIPChat() {

  }

  exitVoIPChat() {

  }

  ////////////////////  画面录制器 //////////////////
  createMediaRecorder(canvas, wx_object) {

  }

  ///////////////////// 视频解析 ////////////////////
  createVideoDecoder() {
    return new VideoDecoder()
  }
  ///////////////////// 文件  //////////////////////

  saveFile(wx_object) {
    const tempFilePath = wx_object.tempFilePath
    const filePath = wx_object.filePath
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getFileSystemManager() {
    return new FileSystemManager(this.fn_global())
  }

  getFileInfo(wx_object) {
    const filePath = wx_object.filePath
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getSavedFileList(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  openDocument(wx_object) {
    const filePath = wx_object.filePath
    const fileType = wx_object.fileType
    const fileName = wx_object.fileName
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  removeSavedFile(wx_object) {
    const filePath = wx_object.filePath
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  //////////////////// 地理位置 ////////////////////


  //////////////////// 设备 ////////////////////////
  /**外围设备 */
  onBLEPeripheralConnectionStateChanged(callback) {
    this.BLEP = () => {
      callback({
        errMsg: 'html5 is not support onBLEPeripheralConnectionStateChanged'
      })
    }
  }

  offBLEPeripheralConnectionStateChanged( /*callback*/ ) {
    this.BLEP = null
  }

  createBLEPeripheralServer(wx_object) {
    return new BLEPeripheralServer(wx_object)
    // console.warn('html5 is not support createBLEPeripheralServer!')
  }

  /** iBeacon */
  stopBeaconDiscovery() {
    console.warn('html5 is not support iBeacon!')
  }

  startBeaconDiscovery() {
    console.warn('HTML5 is not support iBeacon!')
  }

  onBeaconUpdate() {
    console.warn('HTML is not support iBeacon!')
  }

  onBeaconServiceChange() {
    console.warn('HTML is not support iBeacon!')
  }

  offBeaconUpdate() {
    console.warn('HTML is not support iBeacon!')
  }

  offBeaconServiceChange() {
    console.warn('HTML is not support iBeacon!')
  }

  getBeacons() {
    console.warn('HTML is not support iBeacon!')
  }

  /**NFC */
  stopHCE() {
    console.error('html5 is not support NFC')
  }

  startHCE() {
    console.warn('html5 is not support NFC')
  }

  sendHCEMessage() {
    console.warn('HTML5 is not support NFC!')
  }

  onHCEMessage() {
    console.warn('HTML5 is not support NFC!')
  }

  offHCEMessage() {
    console.warn('HTML5 is not support NFC!')
  }

  getNFCAdapter() {
    console.warn('HTML5 is not support NFC!')
  }

  getHCEState() {
    console.warn('HTML5 is not support NFC!')
  }

  /** WIFI */
  stopWifi() {
    console.warn('HTML5 is not support WIFI')
  }

  startWifi() {
    console.warn('HTML5 is not support WIFI')
  }

  setWifiList() {
    console.warn('HTML5 is not support WIFI')
  }

  onWifiConnected() {
    console.warn('HTML5 is not support WIFI')
  }

  onGetWifiList() {
    console.warn('HTML5 is not support WIFI')
  }

  offWifiConnected() {
    console.warn('HTML5 is not support WIFI')
  }

  offGetWifiList() {
    console.warn('HTML5 is not support WIFI')
  }

  getWifiList() {
    console.warn('HTML5 is not support WIFI')
  }

  getConnectedWifi() {
    console.warn('HTML5 is not support WIFI')
  }

  connectWifi() {
    console.warn('HTML5 is not support WIFI')
  }

  /** 日历 */
  addPhoneRepeatCalendar(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  addPhoneCalendar(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /**联系人 */
  // TODO: 未改未测试
  // HACK: 应该不能通过web方式实现
  addPhoneContact(wx_object) {
    const phoneNumber = wx_object.phoneNumber
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /**无障碍 */
  checkIsOpenAccessibility(wx_object) {
    const {
      success,
      fail,
      complete
    } = wx_object
    PROMISE(SUCCESS => {
      const res = {
        open: false
      }
      SUCCESS(res)
    }, success, fail, complete)
  }

  /**低功耗蓝牙 */
  writeBLECharacteristicValue() {
    console.warn('html5 is not support blue tooth')
  }

  setBLEMTU() {
    console.warn('html5 is not support blue tooth')
  }

  readBLECharacteristicValue() {
    console.warn('html5 is not support blue tooth')
  }

  onBLEConnectionStateChange() {
    console.warn('html5 is not support blue tooth')
  }

  onBLECharacteristicValueChange() {
    console.warn('html5 is not support blue tooth')
  }

  offBLEConnectionStateChange() {
    console.warn('html5 is not support blue tooth')
  }

  offBLECharacteristicValueChange() {
    console.warn('html5 is not support blue tooth')
  }

  notifyBLECharacteristicValueChange() {
    console.warn('html5 is not support blue tooth')
  }

  makeBluetoothPair() {
    console.warn('html5 is not support blue tooth')
  }

  getBLEDeviceServices() {
    console.warn('html5 is not support blue tooth')
  }

  getBLEDeviceRSSI() {
    console.warn('html5 is not support blue tooth')
  }

  getBLEDeviceCharacteristics() {
    console.warn('html5 is not support blue tooth')
  }

  createBLEConnection() {
    console.warn('html5 is not support blue tooth')
  }

  createBLEConnection() {
    console.warn('html5 is not support blue tooth')
  }

  closeBLEConnection() {
    console.warn('html5 is not support blue tooth')
  }



  getNetworkType(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onNetworkStatusChange(callback) {
    const connection = navigator.connection
    const connectionInfo = {}
    connectionInfo.isOnline = true
    connectionInfo.networkType = connection.type || 'unknown'
    connection.addEventListener('change', () => {
      if (connection.type === 'cellular') {
        if (connection.rtt < 270) {
          connectionInfo.networkType = '4g'
        } else if (270 <= connection.rtt < 1400) {
          connectionInfo.networkType = '3g'
        } else if (1400 <= connection.rtt) {
          connectionInfo.networkType = '2g'
        } else {
          connectionInfo.networkType = 'unknown'
        }
      }
      if (!navigator.onLine) {
        connectionInfo.networkType = 'none'
        connectionInfo.isOnline = false
      }
      callback(connectionInfo)
    })
  }

  getSystemInfoSync() {
    try {
      return {
        "model": "iPhone 5",
        "pixelRatio": 2,
        "windowWidth": 320,
        "windowHeight": 456,
        "system": "iOS 10.0.1",
        "language": "zh_CN",
        "version": "7.0.4",
        "screenWidth": 320,
        "screenHeight": 568,
        "SDKVersion": "2.16.0",
        "brand": "devtools",
        "fontSizeSetting": 16,
        "benchmarkLevel": 1,
        "batteryLevel": 99,
        "statusBarHeight": 20,
        "safeArea": {
          "top": 20,
          "left": 0,
          "right": 320,
          "bottom": 568,
          "width": 320,
          "height": 548
        },
        "deviceOrientation": "portrait",
        "platform": "devtools",
        "enableDebug": false,
        "devicePixelRatio": 2
      }
    } catch (e) {
      console.error(e)
      throw new Error('getSystemInfoSync:fail')
    }
  }

  getSystemInfo(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }


  onAccelerometerChange(callback) {
    this.accleration = function _eventListener(e) {
      //console.log(arguments[0])
      const accelerationdata = arguments[0].accelerationIncludingGravity
      let res = {
        x: accelerationdata.x,
        y: accelerationdata.y,
        z: accelerationdata.z
      }
      callback(res)
    }
    window.addEventListener('devicemotion', this.accleration, false)
  }

  startAccelerometer(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)

  }

  stopAccelerometer(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onCompassChange(callback) {
    this.compasschange_callback = function compassevent_callbak() {
      const duration = 360 - arguments[0].alpha
      callback(duration)
    }

    window.addEventListener('deviceorientation', this.compasschange_callback, false)
  }

  startCompass(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  stopCompass(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  makePhoneCall(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)

  }

  scanCode(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getClipboardData(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setClipboardData(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setKeepScreenOn(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onUserCaptureScreen(callback) {
    this.captureScreen_callback = function event_captureScreen() {
      const key = arguments[0]
      if (key.altKey && key.key === 'a') {
        callback()
      }
      if (key.key == 's' && key.shiftKey) {
        callback()
      }
    }
    window.addEventListener('keyup', this.captureScreen_callback)
  }

  offUserCaptureScreen(callback) {
    window.removeEventListener('keyup', this.captureScreen_callback)
    callback()
  }

  getScreenBrightness(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setScreenBrightness() {
    console.warn('h5 is not support setScreenBrightness')
  }

  vibrateShort(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  vibrateLong(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onMemoryWarning(callback) {
    function _sourceful() {
      if (!window.performance.memory) throw Error('your browser is not support onMemoryWarning')
      const res = {}
      res.level = 1
      const memoryInfo = window.performance.memory
      const totalJSHeapSize = memoryInfo.totalJSHeapSize
      const usedJSHeapSize = memoryInfo.usedJSHeapSize
      const remainJsHeapSize = totalJSHeapSize - usedJSHeapSize
      const MEMORY_MODERATE = totalJSHeapSize * 0.15
      const MEMORY_LOW = totalJSHeapSize * 0.1
      const MEMORY_CRITICAL = totalJSHeapSize * 0.05
      if (MEMORY_LOW < remainJsHeapSize <= MEMORY_MODERATE) {
        res.level = 5
      } else if (MEMORY_CRITICAL < remainJsHeapSize <= MEMORY_LOW) {
        res.level = 10
      } else if (remainJsHeapSize <= MEMORY_CRITICAL) {
        res.level = 15
      } else {
        res.level = ''
      }
      callback(res)
    }

    performance.onresourcetimingbufferful = _sourceful
  }

  ////////////////// 位置  ////////////////////
  stopLocationUpdate() {}

  startLocationUpdateBackground() {}

  startLocationUpdate() {}

  openLocation(wx_object) {
    const latitude = wx_object.latitude // （必填） 纬度，浮点数，范围为90 ~ -90
    const longitude = wx_object.longitude // （必填）经度，浮点数，范围为180 ~ -180
    // TODO: 5~18 转换为 1~28
    const scale = wx_object.latitude || 28 // 地图缩放级别,整形值,范围从1~28。默认为最大【小程序：缩放比例，范围5~18】
    const name = wx_object.name // 位置名
    const address = wx_object.address // 地址详情说明
    const infoUrl = wx_object.infoUrl // * 在查看位置界面底部显示的超链接,可点击跳转
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onLocationChange() {}

  offLocationChange() {}

  getLocation(wx_object) {
    // const type = wx_object.type
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /////////////////////////////////////////////////
  setInnerAudioOption() {}
  getAvailableAudioSources() {}


  getBackgroundAudioPlayerState(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  playBackgroundAudio(wx_object) {
    const dataUrl = wx_object.dataUrl
    const title = wx_object.title
    const coverImgUrl = wx_object.coverImgUrl
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  pauseBackgroundAudio(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  stopBackgroundAudio(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  seekBackgroundAudio(wx_object) {
    const position = wx_object.position
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onBackgroundAudioPlay(callback) {
    setTimeout(function () {
      const xsw_audio = document.getElementById('xsw_autoplayId')
      if (xsw_audio) {
        xsw_audio.addEventListener('playing', function () {
          const audioStatus = '1'
          callback(audioStatus)
        })
      }
    })
  }

  onBackgroundAudioPause(callback) {
    let audioStatus
    const zzz = setInterval(function () {
      const xsw_audio = document.getElementById('xsw_autoplayId')
      if (xsw_audio) {
        xsw_audio.addEventListener('pause', function () {
          if (xsw_audio.currentTime == 0) {
            audioStatus = '2'
          } else {
            audioStatus = '0'
          }
        })
        const panStatus = '0'
        if (panStatus == audioStatus) {
          callback(audioStatus)
          clearInterval(zzz)
        }
      }
    }, 1000)
  }

  onBackgroundAudioStop(callback) {
    let audioStatus
    const zzz = setInterval(function () {
      const xsw_audio = document.getElementById('xsw_autoplayId')
      if (xsw_audio) {
        xsw_audio.addEventListener('pause', function () {
          if (xsw_audio.currentTime == 0) {
            audioStatus = '2'
          } else {
            audioStatus = '0'
          }
        })
        const panStatus = '2'
        if (panStatus == audioStatus) {
          callback(audioStatus)
          clearInterval(zzz)
        }
      }
    }, 1000)
  }
  // BackgroundAudioManager

  // LivePusher



  // CameraFrameListener

  // EditorContext

  // share

  getSavedFileList() {}

  getSavedFileInfo() {}

  removeSavedFile() {}


  createCameraContext() {}

  login() {
    const url = `https://m.v.qq.com/x/m/play?cid=mzc00200fh3z7x3&vid=x0036mfb4uf&rv=1614333979549`
    const loginUrl = `https://open.weixin.qq.com/sns/explorer_broker?appid=wx68ff4e3ba9f84df4&redirect_uri=https://film.qq.com/weixin/login.html?ru=https%3A%2F%2Fm.v.qq.com%2Fx%2Fm%2Fplay%3Fcid%3Dmzc00200fh3z7x3%26vid%3Dx0036mfb4uf%26rv%3D1614333979549&success=1&type=wx&back=0&nochange=0&state=h5login&response_type=code&scope=snsapi_userinfo&connect_redirect=1#wechat_redirect`

  }


  checkSession() {}

  reportMonitor() {}

  reportAnalytics() {

  }

  requestPayment(wx_object) {
    // 小程序参数
    const timestamp = wx_object.timestamp // 时间戳，从 1970 年 1 月 1 日 00:00:00 至今的秒数，即当前的时间
    const nonceStr = wx_object.nonceStr // 随机字符串，长度为32个字符以下
    const package_s = wx_object.package // 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=***（ package 为js关键词，所以取名为 package_s ）
    const signType = wx_object.signType // 签名算法
    const paySign = wx_object.paySign // 签名
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }


  openSetting() {}

  getSetting() {}

  // Address

  addCard(wx_object) {
    const cardList = wx_object.cardList
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  openCard(wx_object) {
    const cardList = wx_object.cardList
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  checkIsSupportSoterAuthentication() {}

  startSoterAuthentication() {}

  checkIsSoterEnrolledInDevice() {}

  getWeRunData() {}


  startWiFi() {}

  stopWiFi() {}

  connectWiFi() {}

  getWiFiList() {}

  onGetWiFiList() {}

  setWiFiList() {}

  onWiFiConnected() {}

  openBluetoothAdapter() {}

  closeBluetoothAdapter() {}

  getBluetoothAdapterState() {}

  onBluetoothAdapterStateChange() {}

  startBluetoothDevicesDiscovery() {}

  stopBluetoothDevicesDiscovery() {}

  getBluetoothDevices() {}

  getConnectedBluetoothDevices() {}

  onBluetoothDeviceFound() {}

  createBLEConnection() {}

  closeBLEConnection() {}

  getBLEDeviceServices() {}

  getBLEDeviceCharacteristics() {}

  readBLECharacteristicValue() {}

  writeBLECharacteristicValue() {}

  notifyBLECharacteristicValueChange() {}

  onBLEConnectionStateChange() {}

  onBLECharacteristicValueChange() {}



  getBatteryInfo(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  getBatteryInfoSync(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  captureScreen() {

  }



  startDeviceMotionListening(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  stopDeviceMotionListening(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onDeviceMotionChange(callback) {
    this.fn_global().DeviceMotioncallback = callback
  }

  startGyroscope(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  stopGyroscope(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onGyroscopeChange(callback) {
    this.fn_global().Gyroscopecallback = callback
  }

  scanItem() {}

  createWorker() {}

  getExtConfig() {}

  getExtConfigSync() {}


  createIntersectionObserver() {}

  createRewardedVideoAd() {}
  createInterstitialAd() {}

  color() {} // canvas
  ble() {}
  fileSystem() {}
  livePlayer() {}
  livePusher() {}
  mediaContainer() {}
  accountInfo() {}
  chooseAddress() {}
  authorize() {}
  chooseInvoiceTitle() {}
  chooseInvoice() {}
  navigateToMiniProgram() {}
  navigateBackMiniProgram() {}
  UserInfo() {}
  getUserInfo() {}

  ///////////////// 转发 ////////////////////////////
  updateShareMenu() {}

  showShareMenu() {}

  showShareImageMenu() {}

  onCopyUrl() {}

  offCopyUrl() {}

  hideShareMenu() {}

  getShareInfo() {}

  authPrivateMessage() {}


  playVoice() {}
  pauseVoice() {}
  stopVoice() {}

  onKeyboardHeightChange() {
    console.warn('onKeyboardHeightChange are not currently supported')
  }

  offKeyboardHeightChange() {
    console.warn('offKeyboardHeightChange are not currently supported')
  }

  hideKeyboard() {
    console.warn('offKeyboardHeightChange are not currently supported')
  }

  getSelectedTextRange() {
    console.warn('getSelectedTextRange are not currently supported')
  }

  getMenuButtonBoundingClientRect() {
    console.warn('getMenuButtonBoundingClientRect are not currently supported')
  }

  setTopBarText() {
    console.warn('setTopBarText are not currently supported ')
  }

  nextTick(callback) {
    this.fn_global.$nextTick(callback)
  }

  setWindowSize() {
    console.warn('setWindowSize are not currently supported!')
  }

  onWindowResize() {
    console.warn('onWindowResize are not currently supported')
  }

  offWindowResize() {
    console.warn('offWindowResize are not currently supported')
  }
  appHide_callback() {
    let wx_res
    if (document.hidden) {
      wx_res = {
        errMsg: 'onAppHide:ok',
        path: location.href,
        query: {},
        referrerInfo: {},
        scene: 0,
        shareTicket: undefined,
      }
      if (Event.callback) {
        Event.callback(wx_res)
      }
    }
  }

  error_callback(e) {
    if (e) {
      if (Event.callback) {
        Event.callback(e.error)
      }
    }
  }


  setRealtimeManager() {

  }
  setLogManager() {

  }
  createSelectorQuery() {
    return new SelectorQuery(this);
  }

  setNavigationBarTitle(wx_object) {
    const wx_title = wx_object.title
    const current = OneKit.current()

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)

  }

  showNavigationBarLoading(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideNavigationBarLoading(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideHomeButton(wx_object) {

    console.warn('is not currently supported')

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setNavigationBarColor(wx_object) {

    const wx_frontColor = wx_object.frontColor
    const wx_backgroundColor = wx_object.backgroundColor
    // const wx_animation = wx_object.animation

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)

  }

  setBackgroundTextStyle(wx_object) {
    console.warn('is not currently supported')

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setBackgroundColor(wx_object) {
    console.warn('is not currently supported')

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setTabBarBadge(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  removeTabBarBadge(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  showTabBarRedDot(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideTabBarRedDot(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setTabBarStyle(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  setTabBarItem(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  showTabBar(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideTabBar(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  loadFontFace(wx_object) {


    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  startPullDownRefresh(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  stopPullDownRefresh(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  onPullDownRefresh(callback) {
    this.onPullDownRefresh = callback;
  }

  pageScrollTo(wx_object) {
    const wx_scrollTop = wx_object.scrollTop;
    //const wx_duration = wx_object.duration || 300

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /**路由 */

  switchTab(wx_object) {
    let wx_url = wx_object.url;
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  reLaunch(wx_object) {
    let wx_url = wx_object.url;
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  redirectTo(wx_object) {
    let wx_url = wx_object.url;
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  async navigateTo(wx_object) {
    const wx_url = wx_object.url;
    const wx_events = wx_object.events;
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE(async (SUCCESS) => {
      const page = new NodejsPage(wx_url, this)
      await page.run();
      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  navigateBack(wx_object) {
    const wx_delta = wx_object ? (wx_object.delta || 1) : 1; // 返回的页面数
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  /**界面 */
  showToast(wx_object) {
    let tipTxt = wx_object.title;
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)


  }

  showModal(wx_object) {
    let title = wx_object.title;
    let content = wx_object.content;
    let showCancel = wx_object.showCancel;
    let cancelColor = wx_object.cancelColor;
    let confirmColor = wx_object.confirmColor;

    let cancelText;
    if (!wx_object.cancelText) {
      cancelText = "取消";
    } else {
      cancelText = wx_object.cancelText;
    }
    let confirmText;
    if (!wx_object.confirmText) {
      confirmText = "确定";
    } else {
      confirmText = wx_object.confirmText;
    }

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)

  }

  showLoading(wx_object) {
    let tipTxt = wx_object.title;
    let mask;
    if (!wx_object.mask) {
      mask = false
    } else {
      mask = wx_object.mask
    }
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  showActionSheet(wx_object) {
    let itemList = wx_object.itemList;
    let itemColor = wx_object.itemColor;

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideToast(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  hideLoading(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  enableAlertBeforeUnload(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  disableAlertBeforeUnload(wx_object) {

    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  //////////// 地图 ////////////
  createMapContext(mapId) {
    return new MapContext(mapId)
  }
  //////////// 画布 ////////////
  createContext() {
    return new Context()
  }
  createCanvasContext(id) {
    const eCanvas = document.getElementById(id)

    const computedStyle = window.getComputedStyle(eCanvas)
    const width = computedStyle.width
    const height = computedStyle.height
    eCanvas.width = width.substr(0, width.length - 2)
    eCanvas.height = height.substr(0, height.length - 2)
    const canvasContext = eCanvas.getContext("2d")
    return new CanvasContext(canvasContext, eCanvas)
  }

  createOffscreenCanvas() {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  canvasToTempFilePath(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  canvasPutImageData(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  canvasGetImageData(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }


  ////////////////// 相机 //////////////////////////
  createCameraContext() {
    const camaraContext = document.getElementsByTagName('video')
    return new CameraContext(camaraContext)
  }

  //////////////////// 图片  ///////////////////////
  previewMedia(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  //////////////////// 视频  ///////////////////////
  openVideoEditor(wx_object) {
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }

  //////////////////  Editor ////////////////////////
  createEditorContext(id) {
    const editor = document.getElementById(id)
    return new EditorContext(editor)
  }

  drawCanvas(wx_object) {
    let canvasId = wx_object.canvasId;
    let actions = wx_object.actions;
    // let reserve = wx_object.reserve;
    wx_object = null
    ///////////////////
    const eCanvas = document.getElementById(canvasId)
    const computedStyle = window.getComputedStyle(eCanvas)
    const width = computedStyle.width
    const height = computedStyle.height
    eCanvas.width = width.substr(0, width.length - 2)
    eCanvas.height = height.substr(0, height.length - 2)
    let context = eCanvas.getContext("2d");
    let wx_success = wx_object.success;
    let wx_fail = wx_object.fail;
    let wx_complete = wx_object.complete;
    wx_object = null
    return PROMISE((SUCCESS) => {

      const wx_res = {}
      SUCCESS(wx_res);
    }, wx_success, wx_fail, wx_complete)
  }
}
export default new WX()

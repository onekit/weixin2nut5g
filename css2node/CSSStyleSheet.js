import StyleSheet from "./StyleSheet.js"
import TheKit from "nodejs-thekit"
import CSSStyleRule from "./CSSStyleRule.js"
const {
  PATH,
  FILE
} = TheKit
export default class CSSStyleSheet extends StyleSheet {

  constructor(prev) {
    super(prev);
    this.vars = {}
    this._cssRules = [];
  }

  get cssRules() {
    return this._cssRules;
  }
  set cssRules(cssRules) {
    this._cssRules = cssRules;
  }
  /*
  get keyframes() {
    return this._keyframes;
  }
  set keyframes(keyframes) {
    this._keyframes = keyframes;
  }*/
  loadJSON(jSON, path) {
    const addRules = (json) => {
      const cssStyleRules = []
      const selectorTexts = json.value.split(",")
      for (const selectorText of selectorTexts) {
        const cssStyleRule = new CSSStyleRule;
        cssStyleRule.selectorText = selectorText.trim()
        const children = json.children
        for (let v = 0; v < children.length; v++) {
          const child = children[v]
          if (child.type != "Css") {
            continue
          }
          const jsn = child.css;
          if (jsn.key.startsWith("--")) {
            this.vars[jsn.key] = jsn.value.trim()
            continue
          }
          const priority = jsn.priority
          cssStyleRule.style.setProperty(jsn.key, jsn.value.trim(), priority != null && priority === "important" ? 10000 : 0);
        }
        cssStyleRules.push(cssStyleRule)
      }
      return cssStyleRules;
    }
    for (const json of jSON) {
      switch (json.type) {
        case "Selector":
          const rules = addRules(json);
          this.cssRules = this.cssRules.concat(rules);
          break;
        case "Import":
          const url2 = json.value;
          this.load(path, url2);
          break
        case "Media":
          /*
          let match = true
          const media = json.value.substring(json.value.indexOf("@media") + "@media".length)
          const querys = media.split(",")
          for (const query of querys) {
            const pairs = query.trim().split(" and")
            const device = (querys.length == 1 ? "all" : pairs[0].trim())
            //  //console.log("device", device)
            let str = (querys.length == 1 ? pairs[0].trim() : pairs[1].trim())
            str = str.substring(str.indexOf("(") + 1, str.lastIndexOf(")"))
            const features = str.split(",")
            // //console.log(media,features)
            for (const feature of features) {
              const condition_value = feature.split(":")
              const condition = (condition_value.length == 1 ? "none" : condition_value[0].trim())
              const value = (condition_value.length == 1 ? condition_value[0].trim() : condition_value[1].trim())
              switch (condition) {
                case "none":
                  break
                case "prefers-color-scheme":
                  match = false
                  break;
                case "min-width":
                  match = false
                  break;
                default:
                  throw new Error(condition);
              }
            }
          }
          if (match) {
            for (const jsn of json.children) {
              this.cssRules.push(addRule(jsn));
            }
          }*/
          break
        case "Keyframes":
          break
        default:
          throw new Error(json.type);
      }
    }
  }
  load( url,currentUrl) {

    try {
      const path = PATH.rel2abs(currentUrl, url);
      let jSON
      try {
        jSON = JSON.parse(FILE.readString(this.prev + path + '.json'));
      } catch {
        jSON = []
      }
      /*
      ////////////////////////
      this.keyframes(jSON[keyframes]);
      const jsons = jSON.css
      for (let j = 0; j < jsons.length; j++) {
        const json = jsons.get(j);
        letcssStyleRule = new CSSStyleRule;
        if (json[key] === "import") {
          url2 = json[value]
          this.load(path, url2);
          continue;
        }
        cssStyleRule.selectorText(json[key]);
        values = json[value]
        for (let v = 0; v < values.length; v++) {
          jsn = values.get(v);
          priority = jsn[priority]
          cssStyleRule.style.setProperty(jsn[key], jsn[value], priority != null && priority === "important" ? 10000 : 0);
        }
        this.cssRules.push(cssStyleRule);
      }*/
      return this.loadJSON(jSON, path);
    } catch (ex) {
      //ex.printStackTrace();
      console.error("[Onekit CSS?]=========", ex);
      //ex.printStackTrace();
      return false;
    }
  }
}

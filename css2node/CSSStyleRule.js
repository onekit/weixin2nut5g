import CSSRule from "./CSSRule.js"
import CSSStyleDeclaration from "./CSSStyleDeclaration.js"
export default class CSSStyleRule extends CSSRule {

  constructor() {
    super()
    this._style = new CSSStyleDeclaration();
  }

  get selectorText() {
    return this._selectorText;
  }

  set selectorText(selectorText) {
    this._selectorText = selectorText;
  }


  get style() {
    return this._style;
  }
}

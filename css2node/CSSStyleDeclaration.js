import TheKit from "nodejs-thekit";
const {
  COLOR,
  STRING,
  ARRAY
} = TheKit
import OnekitCSS from "./core/OnekitCSS.js";
export default class CSSStyleDeclaration {

  get cssText() {
    return this._cssText;
  }

  set cssText(cssText) {
    this._cssText = cssText;
  }

  get parentRule() {
    return null;
  }

  ////////////////////////////


  constructor() {
    this.propertyNames = [];
    this.propertyValues = {};
    this.propertyPriorities = {};

  }
  toString() {
    const aString = new StringBuilder();
    aString += new Date().getTime() + "===============\r\n";
    for (const name of this.propertyNames) {
      aString += `${name}: ${this.propertyValues[name]}\r\n`;
    }
    return aString.toString();
  }

  get length() {
    return this.propertyNames.length;
  }

  getPropertyPriority(property) {
    property = property.toLowerCase();
    if (!this.propertyPriorities[property]) {
      return 0;
    }
    return this.propertyPriorities[property];
  }

  getPropertyValue(property) {
    property = property.toLowerCase();
    if (!this.propertyValues[property]) {
      return null;
    }
    return this.propertyValues[property];
  }

  item(index) {
    return this.propertyNames[index];
  }

  _setValue(value, propertyName, priority) {
    if (STRING.isEmpty(propertyName)) {
      return;
    }
    propertyName = propertyName.toLowerCase();
    const priority0 = this.getPropertyPriority(propertyName);
    if (priority0 > priority) {
      return;
    }
    if (!this.propertyNames.includes(propertyName)) {
      this.propertyNames.push(propertyName);
    }
    this.propertyValues[propertyName] = value;
    this.propertyPriorities[propertyName] = priority;
    //
    this[STRING.toCamelCase(propertyName)] = value;
  }



  setProperty(key, css, priority) {
    key = key.toLowerCase();
    const prefixes = ["-webkit-", "-moz-", "-ms-", "-o-"];
    for (const prefix of prefixes) {
      if (key.startsWith(prefix)) {
        key = key.substring(prefix.length);
        break;
      }
    }
    switch (key) {
      case "margin": {
        const margins = this.fixArray(css);
        this._setValue(margins[0], "margin-top", priority);
        this._setValue(margins[1], "margin-right", priority);
        this._setValue(margins[2], "margin-bottom", priority);
        this._setValue(margins[3], "margin-left", priority);
      }
      break;
    case "padding": {
      const paddings = this.fixArray(css);
      this._setValue(paddings[0], "padding-top", priority);
      this._setValue(paddings[1], "padding-right", priority);
      this._setValue(paddings[2], "padding-bottom", priority);
      this._setValue(paddings[3], "padding-left", priority);
    }
    break;
    case "background": {
      const array = css.split(" ");
      const positon = "";
      for (const tmp of array) {
        const item = tmp.trim();
        if (item == "inherit") {
          Log.e("==========", "background " + css);
          continue;
        }
        if (ARRAY.contains(["scroll", "fixed"], item)) {
          this._setValue(item, "background-attachment", priority);
          continue;
        }
        if (item.startsWith("url(") && item.endsWith(")")) {
          this._setValue(item, "background-image", priority);
          continue;
        }
        if (ARRAY.contains(["top", "bottom", "left", "right", "center"], item)) {
          positon += item;
          continue;
        }
        if (ARRAY.contains(["repeat", "repeat-x", "repeat-y", "no-repeat"], item)) {
          this._setValue(item, "background-repeat", priority);
          continue;
        }
        if (ARRAY.contains(["border-box", "padding-box", "content-box"], item)) {
          this._setValue(item, "background-origin", priority);
          continue;
        }
        if (ARRAY.contains(["border-box", "padding-box", "content-box"], item)) {
          this._setValue(item, "background-clip", priority);
          continue;
        }
        const temp = OnekitCSS.share.View_H5.getSize(item, item);
        if (temp != null && temp[0] == "px") {
          this._setValue(item, "background-size", priority);
          continue;
        }
        const color = COLOR.color(item);
        if (color != null) {
          this._setValue(item, "background-color", priority);
        }
      }
      if (positon.length > 0) {
        this._setValue(positon.toString().trim(), "background-position", priority);
      }
    }
    break;
    case "border": {
      this._applyStyle_border("top", css, priority);
      this._applyStyle_border("right", css, priority);
      this._applyStyle_border("bottom", css, priority);
      this._applyStyle_border("left", css, priority);
    }
    break;
    case "border-left": {
      this._applyStyle_border("left", css, priority);
    }
    break;
    case "border-right": {
      this._applyStyle_border("right", css, priority);
    }
    break;
    case "border-top": {
      this._applyStyle_border("top", css, priority);
    }
    break;
    case "borderbottom": {
      this._applyStyle_border("bottom", css, priority);
    }
    break;
    case "border-image": {

      const array = css.split(" ");
      for (const tmp of array) {
        item = tmp.trim();
        if (item == "inherit") {
          Log.e("=================", "border-image" + css);
          continue;
        }
        if (ARRAY.contains(["stretch", "repeat", "round"], item)) {
          this._setValue(item, "border-image-repeat", priority);
          continue;
        }
        if (item.startsWith("url(") && item.endsWith(")")) {
          this._setValue(item, "border-image-source", priority);
          continue;
        }
        const temp = OnekitCSS.share.View_H5.getSize(key, item);
        if (temp != null && temp[0] == "px") {
          this._setValue(item, "border-image-slice", priority);
          continue;
        }
        const color = COLOR.color(item);
        if (color != null) {
          this._setValue(item, "border-image-color", priority);
        }
      }
    }
    break;
    case "border-radius": {
      const csses = this.fixArray(css);
      this._setValue(csses[0], "border-top-left-radius", priority);
      this._setValue(csses[1], "border-top-right-radius", priority);
      this._setValue(csses[2], "border-bottom-left-radius", priority);
      this._setValue(csses[3], "border-bottom-right-radius", priority);
    }
    break;
    case "border-color": {
      const csses = this.fixArray(css);
      this._setValue(csses[0], "border-top-color", priority);
      this._setValue(csses[1], "border-right-color", priority);
      this._setValue(csses[2], "border-bottom-color", priority);
      this._setValue(csses[3], "border-left-color", priority);
    }
    break;
    case "border-style": {
      const csses = this.fixArray(css);
      this._setValue(csses[0], "border-top-style", priority);
      this._setValue(csses[1], "border-right-style", priority);
      this._setValue(csses[2], "border-bottom-style", priority);
      this._setValue(csses[3], "border-left-style", priority);
    }
    break;
    case "border-width": {
      const csses = this.fixArray(css);
      this._setValue(csses[0], "border-top-width", priority);
      this._setValue(csses[1], "border-right-width", priority);
      this._setValue(csses[2], "border-bottom-width", priority);
      this._setValue(csses[3], "border-left-width", priority);
    }
    break;
    case "flex": {
      if (!css == "none") {
        const array = css.split(" ");
        for (let i = 0; i < array.length; i++) {
          tmp = array[i];
          item = tmp.trim();
          switch (i) {
            case 0:
              this._setValue(item, "flex-grow", priority);
              break;
            case 1:
              this._setValue(item, "flex-shrink", priority);
              break;
            case 2:
              this._setValue(item, "flex-basis", priority);
              break;
            default:
              break;
          }
        }
      }
    }
    break;
    case "flex-flow": {
      const array = css.split(" ");
      for (let i = 0; i < array.length; i++) {
        tmp = array[i];
        item = tmp.trim();
        switch (i) {
          case 0:
            this._setValue(item, "flex-direction", priority);
            break;
          case 1:
            this._setValue(item, "flex-wrap", priority);
            break;
          default:
            break;
        }
      }

    }
    case "font":
      this._applyStyle_font(css, priority);
      break;
    default: {
      this._setValue(css, key, priority);
    }
    }
  }

  fixArray(aString) {
    const array = aString.split(" ");
    switch (array.length) {
      case 1:
        return [array[0], array[0], array[0], array[0]];
      case 2:
        return [array[0], array[1], array[0], array[1]];
      case 3:
        return [array[0], array[1], array[2], array[1]];
      case 4:
        return array;
      default:
        return array;
    }
  }

  _applyStyle_border(border, css, priority) {
    const array = css.split(" ");
    for (const tmp of array) {
      const item = tmp.trim();
      if (ARRAY.contains(["none", "hidden", "dotted", "dashed", "solid", "float", "groove", "ridge", "", "inset", "outset", "inherit"], item)) {
        this._setValue(item, `border-${border}-width`, priority);
        continue;
      }
      const temp = OnekitCSS.share.View_H5.getSize(border, item);
      if (temp != null && temp[0] == "px") {
        this._setValue(item, `border-${border}-width`, priority);
        continue;
      }
      const color = COLOR.color(item);
      if (color != null) {
        this._setValue(item, `border-${border}-color`, priority);
      }
    }
  }

  _applyStyle_font(css, priority) {
    const array = css.split(" ");
    for (const tmp of array) {
      item = tmp.trim();

      const temp = OnekitCSS.share.View_H5.getSize("font-size", item);
      if (temp != null && temp[0] == "px") {
        this._setValue(item, "font-size", priority);
        continue;
      }
      this._setValue(item, "font-faminy", priority);
    }
  }

}

import TheKit from "nodejs-thekit"
const {
  ARRAY
} = TheKit
export default class View_H5 {
  constructor(OnekitCSS) {
    this.OnekitCSS = OnekitCSS
  }
  getDisplay(node) {
    const display = node.computedStyle.display;
    if (display != null) {
      return display;
    }
    /*String tagName = node.getClass().getSimpleName().toUpperCase();
    if (ARRAY.contains(new String[]{"TEXT"}, tagName)) {
        display = "inline";
    }else if (ARRAY.contains(new String[]{"PAGE","CANVAS","AUDIO","WEB_VIEW","FORM", "VIEW", "TEXTAREA", "BUTTON", "NAVIGATOR", "SCROLL_VIEW", "SWIPER", "SWIPER_ITEM"}, tagName)) {
        display = "block";
    } else if (ARRAY.contains(new String[]{"PICKER_VIEW", "PICKER_VIEW_COLUMN", "CHECKBOX_GROUP", "RADIO_GROUP", "LABEL", "IMAGE", "VIDEO", "ICON", "PROGRESS"}, tagName)) {
        display = "inline-block";
    }else if (node instanceof FORM_ITEM) {
        display = "inline-block";
    } else if (!node.getClass().getName().startsWith("demo.layout.ui.")) {
        display = "inline-block";
    } else {
        Log.e("[getDisplay]", tagName);
        display = "block";
    }
    return display;*/
    return "inline";
  }
  getSize(style, value) {
    // style=style.toLowerCase().replace("-","");
    if (null == value) {
      return ARRAY.contains(["marginLeft", "marginRight", "marginTop", "marginBottom"], style) ? ["px", 0 + ""] : null;
    }
    value = value.trim();
    if (value == "auto") {
      return null;
    }
    if (value == "0") {
      return ["px", 0 + ""];
    }
    const units = ["rpx", "px", "em", "%"];
    for (const item of units) {
      if (!value.endsWith(item)) {
        continue;
      }
      let unit = item;
      let v = parseFloat(value.substring(0, value.length - unit.length));
      if (unit == "rpx") {
        unit = "px";
        v = v * this.OnekitCSS.window.width / 750;
      }
      return [unit, v + ""];
    }
    return null;
  }
}

export default class DOM{
  static isRoot(node){
    return node.parent==null
  }
  static index(node){
    return node.index
  }
}

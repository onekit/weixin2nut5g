import TheKit from "nodejs-thekit";
const {
  STRING,
  ARRAY,
} = TheKit
import DOM from "./DOM.js"
export default class View_CSS {

  constructor(OnekitCSS) {
    this.OnekitCSS = OnekitCSS;
  }
  matchSelector(selectorText, style, SELF) {
    if (STRING.isEmpty(selectorText)) {
      return -1;
    }
    if (selectorText.trim() == "*") {
      return 0;
    }
    const allSelectors = selectorText.split(",");
    for (const s of allSelectors) {
      let allSelector = s.trim();
      while (allSelector.includes(": ")) {
        allSelector = allSelector.replace(": ", ":");
      }
      const priority = this.checkBrotherSelector(allSelector, style, SELF);
      if (priority >= 0) {
        return priority;
      }
    }

    return -1;
  }
  checkBrotherSelector(aString, style, SELF) {
    let flag;
    if (aString.indexOf(">") > 0) {
      flag = ">";
      aString = aString.replace(new RegExp(">", "gm"), " ");
    } else if (aString.indexOf("+") > 0) {
      flag = "+";
      aString = aString.replace("+", " ");
    } else if (aString.indexOf("~") > 0) {
      flag = "~";
      aString = aString.replace("~", " ");
    } else {
      flag = " ";
    }
    //
    let brothers;
    if (aString.indexOf(" ") > 0) {
      brothers = aString.split(" ");
    } else {
      brothers = [aString];
    }
    //
    const temp = [];
    for (const brother of brothers) {
      if (brother == null) {
        continue;
      }
      temp.push(brother);
    }
    brothers = temp;
    //
    let v = SELF;
    let result = 0;
    let matchs = 0;
    for (let b = brothers.length - 1; b >= 0; b--) {
      let brother = brothers[b];
      let priority = this.checkSelector(style, brother, v);
      if (priority < 0) {
        if (b < brothers.length - 1 && ARRAY.contains([" ", "~"], flag)) {
          while (priority < 0) {
            switch (flag) {
              case " ":
                v = v.parent;
                break;
              case "~":
                do {
                  v = v.parent.children[DOM.index(v) - 1];
                } while (v != null && v.constructor.name == "literal");
                break;
              default:
                return -1;

            }
            if (v == null || DOM.isRoot(v)) {
              return -1;
            }
            priority = this.checkSelector(style, brother, v);
            if (priority >= 0) {
              break;
            }
          }
          if (priority < 0) {
            return priority;
          }
        } else {
          return priority;
        }
        return priority;
      }

      result += priority;
      matchs++;


      if (v == null || DOM.isRoot(v)) {
        break;
      }

      switch (flag) {
        case ">":
        case " ":
          v = v.parent;
          break;
        case "+":
        case "~":
          do {
            v = v.parenet.children[DOM.index(v) - 1];
          } while (v != null && v.constructor.name=='literal');
          break;
        default:
          v = null;
          break;

      }
      if (v == null || DOM.isRoot(v)) {
        break;
      }
    }
    if (matchs < brothers.length) {
      return -1;
    } else {
      return result;
    }
  }

  checkSelector(style, filter, SELF) {
    /*filter = filter.replace("::",":");
    String[] filter_beforeafter = filter.split("::");
    beforeafter = null;
    if (filter_beforeafter.length > 1) {
        beforeafter = filter_beforeafter[1].trim();
    }
    filter = filter_beforeafter[0].trim();
    int result = 0;
    if (filter.endsWith("]")) {
        attr = filter.substring(filter.lastIndexOf("[") + 1, filter.lastIndexOf("]") - 1);
        result += checkAttributeSelector(attr, SELF);
        if (result < 0) {
            return -1;
        }
        filter = filter.substring(0, filter.lastIndexOf("["));
    }*/
    filter = filter.replace("::", ":");
    let element_pseudos = filter.split(":");
    let priority = this.checkClassSelector(element_pseudos[0], SELF);
    if (priority < 0) {
      return priority;
    }
    for (let i = 1; i < element_pseudos.length; i++) {
      const pseudo = element_pseudos[i];
      if (!(pseudo == "before") && !(pseudo == "after")) {
        priority = this.checkPseudoElementSelector(style, pseudo, SELF);
        if (priority < 0) {
          return priority;
        }
      } else {
        this.setBeforeafter(pseudo, style, SELF, priority);
        return -1;
      }
    }
    return 10;
  }
  checkPseudoElementSelector(style, filter, SELF) {

    let parentNode;
    /// if(SELF.parent instanceof CssELEMENT_){
    parentNode = SELF.parent;
    ///}else{
    //      parentNode=null;
    // }
    let priority = 10;
    let child;
    let i, N, n;
    if (filter == "only-child") {

      n = 0;
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*
        if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        n++;
      }
      if (n == 1) {
        return priority;
      } else {
        return -1;
      }
    } else if (filter === "first-child") {
      if (parentNode == null) {
        return -1;
      }
      n = 0;
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        /*if (child.constructor.name=='literal') {
          continue;
        }*/
        /*
        if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        if (DOM.index((child)) == DOM.index(SELF)) {
          return priority;
        } else {
          return -1;
        }
      }
    } else if (filter === "only-of-type") {
      if (parentNode == null) {
        return -1;
      }
      n = 0;
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /* if (child instanceof CssBeforeAfter_) {
             continue;
         }*/
        if (child.getClass().getName() === SELF.constructor.name) {
          n++;
        }
      }
      if (n == 1) {
        return priority;
      } else {
        return -1;
      }
    } else if (filter === "last-child") {
      if (parentNode == null) {
        return -1;
      }
      for (i = parentNode.children.length - 1; i >= 0; i--) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*  if (child instanceof CssBeforeAfter_) {
              continue;
          }*/
        if (DOM.index(SELF) == DOM.index((child))) {
          return priority;
        } else {
          return -1;
        }
      }
    } else if (filter.startsWith("nth-child(")) {
      if (parentNode == null) {
        return -1;
      }
      N = Integer.parseInt(filter.substring("nth-child(".length, filter.length - 1));
      n = 0;
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*
        if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        n++;
        if (DOM.index(SELF) == DOM.index((child))) {
          if (n == N) {
            return priority;
          } else {
            return -1;
          }
        }
      }
    } else if (filter.startsWith("nth-last-child(")) {
      if (parentNode == null) {
        return -1;
      }
      N = Integer.parseInt(filter.substring("nth-last-child(".length, filter.length - 1));
      n = 0;
      for (i = parentNode.children.length - 1; i >= 0; i--) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        n++;
        if (DOM.index(SELF) == DOM.index((child))) {
          if (n == N) {
            return priority;
          } else {
            return -1;
          }
        }
      }
    } else if (filter.startsWith("nth-of-type(")) {
      if (parentNode == null) {
        return -1;
      }
      N = Integer.parseInt(filter.substring("nth-of-type(".length, filter.length - 1));
      n = 0;
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /* if (child instanceof CssBeforeAfter_) {
             continue;
         }*/
        if (SELF.getClass().getSimpleName() === child.constructor.name) {
          n++;
          if (DOM.index(SELF) == DOM.index((child))) {
            if (n == N) {
              return priority;
            } else {
              return -1;
            }
          }
        }
      }
    } else if (filter.startsWith("nth-last-of-type(")) {
      if (parentNode == null) {
        return -1;
      }
      N = Integer.parseInt(filter.substring("nth-last-of-type(".length, filter.length - 1));
      n = 0;
      for (i = parentNode.children.length - 1; i >= 0; i--) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*
        if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        if (SELF.getClass().getSimpleName() === child.constructor.name) {
          n++;
          if (DOM.index(SELF) == DOM.index((child))) {
            if (n == N) {
              return priority;
            } else {
              return -1;
            }
          }
        }
      }
    } else if (filter === "first-of-type") {
      if (parentNode == null) {
        return -1;
      }
      for (i = 0; i < parentNode.children.length; i++) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        /*
        if (child instanceof CssBeforeAfter_) {
            continue;
        }*/
        if (SELF.getClass().getSimpleName() === child.constructor.name) {
          if (DOM.index(SELF) == DOM.index((child))) {
            return 10;
          } else {
            return -1;
          }
        }
      }
    } else if (filter === "last-of-type") {
      if (parentNode == null) {
        return -1;
      }
      for (i = parentNode.children.length - 1; i >= 0; i--) {
        child = parentNode.children[i];
        if (child.constructor.name=='literal') {
          continue;
        }
        if (child.getClass().getSimpleName() === SELF.constructor.name) {
          if (DOM.index(SELF) == DOM.index((child))) {
            return priority;
          } else {
            return -1;
          }
        }
      }
    } else if (filter === "root") {
      return priority;
    } else if (filter === "empty") {
      if (SELF.children.length <= 1) {
        return priority;
      } else {
        return -1;
      }

    } else {
      if (filter.startsWith("not(")) {
        filter = filter.substring("not(".length, filter.lastIndexOf(")"));
        return matchSelector(filter, style, SELF) > 0 ? -1 : priority;
      } else {
        Log.e("==========", "_loadStyleSheet-filter " + filter);
        return priority;
      }
    }
    return -1;
  }
  setBeforeafter(beforeafter, style, SELF, priority) {
    /*
            switch (beforeafter) {
                case "before":{
                    let beforeLayoutParams;
                    if(((CssLayoutParams)SELF.getLayoutParams()).before==null){
                        BeforeAfter before = new BeforeAfter(SELF.getContext());
                        beforeLayoutParams = new CssLayoutParams(0,0);
                        beforeLayoutParams.computedStyle.display = "inline";
                        beforeLayoutParams.computedStyle.width = "auto";
                        beforeLayoutParams.computedStyle.height = "auto";
                        before.setLayoutParams(beforeLayoutParams);
                        ((CssLayoutParams)SELF.getLayoutParams()).before=before;
                    }else{
                        beforeLayoutParams=(CssLayoutParams)((CssLayoutParams)SELF.getLayoutParams()).before.getLayoutParams();
                    }
                    OnekitCSS._loadStyle(SELF,style,priority,beforeLayoutParams.computedStyle);
                    return ;}
                case "after":{
                    CssLayoutParams afterLayoutParams;
                    if(((CssLayoutParams)SELF.getLayoutParams()).after==null){
                        BeforeAfter after = new BeforeAfter(SELF.getContext());
                        afterLayoutParams = new CssLayoutParams(0,0);
                        afterLayoutParams.computedStyle.display = "inline";
                        afterLayoutParams.computedStyle.width = "auto";
                        afterLayoutParams.computedStyle.height = "auto";
                        after.setLayoutParams(afterLayoutParams);
                        ((CssLayoutParams)SELF.getLayoutParams()).after=after;
                    }else{
                        afterLayoutParams=(CssLayoutParams)((CssLayoutParams)SELF.getLayoutParams()).after.getLayoutParams();
                    }
                    OnekitCSS._loadStyle(SELF,style,priority,afterLayoutParams.computedStyle);
                    return ;}
                default:
                    Log.e("[setBeforeafter]",beforeafter);
                    return ;
            }*/
  }

  checkAttributeSelector(aString, SELF) {
    //  if(aString.endsWith("]")) {
    //     aString = aString.substring(0, aString.length- 1);
    //  }
    let attributePair;
    flag;
    if (aString.indexOf("~=") > 0) {
      flag = "~=";
      attributePair = aString.split("~=");
    } else if (aString.indexOf("|=") > 0) {
      flag = "|=";
      attributePair = aString.split("|=");
    } else if (aString.indexOf("^=") > 0) {
      flag = "^=";
      attributePair = aString.split("^=");
    } else if (aString.indexOf("*=") > 0) {
      flag = "*=";
      attributePair = aString.split("*=");
    } else if (aString.indexOf("$=") > 0) {
      flag = "$=";
      attributePair = aString.split("$=");
    } else if (aString.indexOf("=") > 0) {
      flag = "=";
      attributePair = aString.split("=");
    } else {
      flag = "";
      attributePair = [aString];
    }
    attributeName = attributePair[0];
    attribute = SELF[attributeName]
    if (null == attribute) {
      return -1;
    }
    if (attributePair.length > 1) {
      attributeValue = attributePair[1];
      attributeValue = attributeValue.substring(1, attributeValue.length - 1);

      if (attributeValue != null) {
        let isMatch;
        switch (flag) {
          case "=":
            isMatch = (attribute === attributeValue);
            break;
          case "~=":
          case "*=":
            isMatch = attribute.includes(attributeValue);
            break;
          case "^=":
          case "|=":
            isMatch = attribute.startsWith(attributeValue);
            break;
          case "$=":
            isMatch = attribute.endsWith(attributeValue);
            break;
          default:
            isMatch = false;
            Log.e("checkAttributeSelector", flag);
            break;
        }
        if (!isMatch) {
          return -1;
        }
      } else {
        return -1;
      }
    }
    return 10;
  }
  checkClassSelector(aString, SELF) {
    let node = SELF;
    let p = aString.indexOf(".");
    if (p >= 0) {
      let className = aString.substring(p + 1);
      if (STRING.isEmpty(node.className)) {
        return -1;
      }
      let classNames = node.className.split(" ");
      if (!ARRAY.contains(classNames, className)) {
        return -1;
      }
      if (p > 0) {
        let priority = checkIdSelector(node, p < 0 ? aString : aString.substring(0, p));
        if (priority < 0) {
          return priority;
        } else {
          return 10 + priority;
        }
      } else {
        return 10;
      }
    }
    return this.checkIdSelector(SELF, p < 0 ? aString : aString.substring(0, p));
  }
  checkIdSelector(SELF, aString) {
    /*if(!(SELF instanceof CssELEMENT_)){
        return -1;
    }*/
    //        node = SELF;
    let layoutParams = SELF
    let p = aString.indexOf("#");
    if (p >= 0) {
      id = aString.substring(p + 1);
      if (id == layoutParams.id) {
        let priority = 0;
        if (p > 0) {
          priority = checkElementSelector(aString.substring(0, p), SELF);
          if (priority < 0) {
            return priority;
          }
        }
        return priority + 100;
      } else {
        return -1;
      }
    }
    return this.checkElementSelector(aString, SELF);
  }
  checkElementSelector(aString, SELF) {
    if (SELF.constructor.name === aString) {
      return 1;
    } else {
      return -1;
    }
  }

}

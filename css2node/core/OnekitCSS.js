import BeforeAfter from "../BeforeAfter.js";
import TheKit from "nodejs-thekit";
const {
  COLOR,
  STRING,
  ARRAY
} = TheKit
//import CSSRule from "../CSSRule.js";
import CSSStyleDeclaration from "../CSSStyleDeclaration.js";
//import CSSStyleRule from "../CSSStyleRule.js";
import CSSStyleSheet from "../CSSStyleSheet.js";
import View_CSS from './View_CSS.js';
import View_H5 from './View_H5.js';
export default class OnekitCSS {


  constructor(window, prev) {
    this.globalStyleSheets = [];
    this.window = window;
    this.View_H5 = new View_H5(this);
    this.View_CSS = new View_CSS(this);
    // View_measure = new View_measure(this);
    // View_layout = new View_layout(this);
    this.prev = prev;
    OnekitCSS.share = this
  }

  init(urls) {
    for (url of urls) {
      const styleSheet = new CSSStyleSheet(prev);
      styleSheet.load(url);
      this.globalStyleSheets.push(styleSheet);

    }
  }

  run(rootView, pageStyleSheetUrls, styleSheetJSON, currentUrl) {

    this.vars = {};
    //LOG LOG =new LOG();
    const pageStyleSheets = [];
    for (const pageStyleSheetUrl of pageStyleSheetUrls) {

      const styleSheet = new CSSStyleSheet(this.prev);
      styleSheet.load(pageStyleSheetUrl, currentUrl);
      pageStyleSheets.push(styleSheet);
    }
    if (styleSheetJSON) {
      const styleSheet = new CSSStyleSheet(this.prev);
      styleSheet.loadJSON(styleSheetJSON, currentUrl)
      pageStyleSheets.push(styleSheet)
    }
    this.css_run(rootView, null, pageStyleSheets);

  }
  /*
    css_measure(SELF) {
      View_measure.measure(SELF);
    }
  */
  css_run(SELF, parentStyle, pageStyleSheets) {
    //   LOG LOG = new LOG(SELF.hashCode()+"");
    const computedStyle = new CSSStyleDeclaration();
    //  if (SELF.getLayoutParams() instanceof CssLayoutParams) {
    //  const layoutParams = SELF.getLayoutParams();
    //
    this._loadParentStyle(SELF, parentStyle, computedStyle);
    for (const styleSheet of this.globalStyleSheets) {
      this._loadStyleSheet(SELF, styleSheet, computedStyle);
    }
    for (const styleSheet of pageStyleSheets) {
      this._loadStyleSheet(SELF, styleSheet, computedStyle);
    }
    const style = SELF.style;
    if (style != null) {
      const styles = style.trim().split(";");
      for (const temp of styles) {
        const pair = temp.trim().split(":");
        if (pair.length < 2) {
          continue;
        }
        const property = pair[0].trim();
        const value = pair[1].trim();
        const p = value.indexOf("!important");
        const priority = 1000;
        if (p >= 0) {
          value = value.substring(0, p).trim();
          priority += 10000;
        }
        computedStyle.setProperty(property, value, priority);
      }

    }
    SELF.computedStyle = computedStyle;
    if (this.View_H5.getDisplay(SELF) == "none") {

      return;
    }
    const viewGroup = SELF;
    for (let i = 0; i < viewGroup.children.length; i++) {
      const child = viewGroup.children[i];
      this.css_run(child, computedStyle, pageStyleSheets);
    }
  }

  _loadStyleSheet(SELF, styleSheet, computedStyle) {
    if (styleSheet == null) {
      return;
    }
    ////////////////////////////////
    this.vars = Object.assign(this.vars, styleSheet.vars);
    /////////////////////////////////////
    const cssRules = styleSheet.cssRules;
    for (let r = 0; r < cssRules.length; r++) {
      const rule = cssRules[r];
      const selectorText = rule.selectorText;
      if (STRING.isEmpty(selectorText)) {
        continue;
      }
      const priority = this.View_CSS.matchSelector(selectorText, rule.style, SELF);

      if (priority >= 0) {
        this._loadStyle(SELF, rule.style, priority, computedStyle);
      }
    }
  }

  _loadParentStyle(node, parentStyle, computedStyle) {
    if (null == parentStyle) {
      return;
    }

    for (let i = 0; i < parentStyle.length; i++) {
      const name = parentStyle.item(i);
      if (name.startsWith("font") ||
        ARRAY.contains([
          "color",
          "text-align",
          "line-height"
        ], name)) {
        const css = this._var(parentStyle.getPropertyValue(name));
        const priority = parentStyle.getPropertyPriority(name);
        computedStyle.setProperty(name, css, priority);
      }
    }

  }
  _var(css) {
    const VAR = "var("
    var p = css.indexOf(VAR);
    if (p < 0) {
      return css;
    }
    var q = css.indexOf(")", p)
    var name = css.substring(p + VAR.length, q)
    return css.substring(0, p) + this.vars[name] + css.substring(q + 1)
  }
  _loadStyle(SELF, style, priority, computedStyle) {
    if (style == null) {
      return;
    }
    for (let i = 0; i < style.length; i++) {
      const name = style.item(i);
      const value = this._var(style.getPropertyValue(name));
      computedStyle.setProperty(name, value, priority);
    }
  }

  getComputedStyle(node) {
    return node.computedStyle;
  }
  /*
    css_layout(view) {

      View_layout.h5css(view);
      if (!(view instanceof ViewGroup)) {
        return;
      }
      const SELF = view;
      for (let i = 0; i < SELF.children.length; i++) {
        child = SELF.children[i];
        if (!(child.getLayoutParams() instanceof CssLayoutParams)) {
          continue;
        }
        if (View_H5.getDisplay(child) === "none")) {
        continue;
      }
      const boundingClientRect = {};
      if (child.constructor.name == "literal") {
        const literal = child;
        literal.view().setTextColor(COLOR.color(View_H5.getColor(SELF)));
        boundingClientRect.leftMargin = boundingClientRect.x;
      boundingClientRect.topMargin = boundingClientRect.y;
    boundingClientRect.width = boundingClientRect.measuredWidth;
  boundingClientRect.height = boundingClientRect.measuredHeight;
  child.setLayoutParams(boundingClientRect);
  continue;
  }
  if (child instanceof BeforeAfter) {
    const beforeAfter = child;
    beforeAfter.setTextColor(COLOR.color(View_H5.getColor(SELF)));
  }
  node = child;
  boundingClientRect.leftMargin = boundingClientRect.x;
  boundingClientRect.topMargin = boundingClientRect.y;
  boundingClientRect.width = boundingClientRect.measuredWidth;
  boundingClientRect.height = boundingClientRect.measuredHeight;
  //Log.e("[CSS]"+new Date().getTime()+" "+child.getClass().getSimpleName(),boundingClientRect.leftMargin+" "+boundingClientRect.topMargin+" "+boundingClientRect.width+" "+boundingClientRect.height);

  child.setLayoutParams(boundingClientRect);
  css_layout(node);
  }
  }*/
}

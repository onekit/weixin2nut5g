export default class CSSRule {

  get cssText() {
    return this._cssText;
  }
  set cssText(cssText) {
    this._cssText = cssText;
  }
  get parentRule() {
    return null;
  }
  get parentStyleSheet() {
    return null;
  }
}

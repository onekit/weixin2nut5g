const acorn = await import("acorn");
const {
  Parser
} = acorn;
//import mustache from "mustache";
class Mustache2 {
  render(template, data) {
    this.data = data
    const ast = Parser.parse(template, {
      sourceType: "module",
      ecmaVersion: 2020
    });
    const result = this.doNode(ast)
    //console.log("mmmmmmm", template, typeof (result), result)
    return result && result != "undefined" ? result : "";
  }
  doNode(node) {
    if (node == null) {
      return null
    }
    switch (node.type) {
      case "Program":
        return this.Program(node);
      case "Identifier":
        return this.Identifier(node);
      case "Literal":
        return this.Literal(node);
      case "BlockStatement":
        return this.BlockStatement(node)
      case "ExpressionStatement":
        return this.ExpressionStatement(node)
      case "MemberExpression":
        return this.MemberExpression(node)
      case "BinaryExpression":
        return this.BinaryExpression(node)
      case "LogicalExpression":
        return this.LogicalExpression(node)
      case "CallExpression":
        return this.CallExpression(node)
      case "UnaryExpression":
        return this.UnaryExpression(node)
      case "ConditionalExpression":
        return this.ConditionalExpression(node);
      default:
        console.log(node)
        throw new Error(node.type);
    }
  }
  Program(node) {
    if (node.body.length == 1) {
      if (node.body[0].type == 'BlockStatement') {
        if (node.body[0].body[0].type == 'BlockStatement') {
          const r = this.doNode(node.body[0].body[0].body[0])
       //   console.log("111111111", node, typeof (r), r)
          return r;
        } else {
          const r = this.doNode(node.body[0].body[0])
         // console.log("2222222222", node, typeof (r), r)
          return r;
        }
      } else {
        const r = this.doNode(node.body[0])
     //   console.log("333333333333", node, typeof (r), r)
        return r;
      }
    }
    let result = "";
    for (const child of node.body) {
      result += `${this.doNode(child)}`
    }
    return result;
  }
  Identifier(node) {
    return this.data[node.name];
  }
  Literal(node) {
    return node.raw;
  }
  BlockStatement(node) {
    let result = "";
    for (const child of node.body) {
      result += `${this.doNode(child)}`
    }
    return result;
  }
  ExpressionStatement(node) {
    return this.doNode(node.expression)
  }
  MemberExpression(node) {
    try {
      const object = this.doNode(node.object)
      if (node.computed) {
        const property = this.doNode(node.property)
        return object[property]
      } else {
        switch (node.property.type) {
          case "Identifier":
            return object[node.property.name]
          case "Literal":
            return object[node.property.value]
          default:
            console.log(node)
            throw new Error(node.type);
        }
      }
    } catch (ex) {
      //console.error(ex)
      return ""
    }
  }
  BinaryExpression(node) {
    const left = this.doNode(node.left)
    const right = this.doNode(node.right)
    switch (node.operator) {
      case ">":
        return left > right
      default:
        return eval(`${left} ${node.operator} ${right}`)
    }
  }
  LogicalExpression(node) {
    const left = this.doNode(node.left)
    const right = this.doNode(node.right)
    switch (node.operator) {
      case "||":
        return left ? left : right;
      default:
        return eval(`${left} ${node.operator} ${right}`)
    }
  }
  ConditionalExpression(node) {
    return this.doNode(this.test) ? this.doNode(this.consequent) : this.doNode(this.alternate)
  }
  UnaryExpression(node) {
    const argument = this.doNode(node.argument)
    const operator = node.operator
    switch (operator) {
      case '!':
        return !argument

      default:
        if (node.prefix) {
          return eval(`${operator}${argument}`)
        } else {
          return eval(`${argument}${operator}`)
        }
        break;
    }
  }
  CallExpression(node) {
    const callee = this.doNode(node.callee)
    //console.log("callee", node.callee, callee)
    const params = []
    if (node.arguments) {
      for (const arg of node.arguments) {
        params.push(this.doNode(arg))
      }
    }
    if (!callee) {
      return ""
    }
    return callee.apply(callee, params)
  }
}
export default new Mustache2()

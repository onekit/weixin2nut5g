import path from "path"
import fs from "fs"
import OneKit from "../x2nut5g/OneKit.js";
const __dirname = path.resolve(path.dirname(''));
export default class LocalStorage {
  static get _path() {
    return path.join(__dirname, "onekit", global.address, "localStorage.json")
  }
  static _init() {
    return fs.existsSync(this._path) ? JSON.parse(fs.readFileSync(this._path)) : {};
  }
  static setItem(key, value) {
    const localStorage = this._init()
    localStorage[key] = value
    fs.writeFileSync(this._path, JSON.stringify(localStorage))
  }
  static getItem(key) {
    const localStorage = this._init()
    return localStorage[key]
  }
}
